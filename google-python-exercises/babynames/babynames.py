#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import sys, re

"""Baby Names exercise

Define the extract_names() function below and change main()
to call it.

For writing regex, it's nice to include a copy of the target
text for inspiration.

Here's what the html looks like in the baby.html files:
...
<h3 align="center">Popularity in 1990</h3>
....
<tr align="right"><td>1</td><td>Michael</td><td>Jessica</td>
<tr align="right"><td>2</td><td>Christopher</td><td>Ashley</td>
<tr align="right"><td>3</td><td>Matthew</td><td>Brittany</td>
...

Suggested milestones for incremental development:
 -Extract the year and print it
 -Extract the names and rank numbers and just print them
 -Get the names data into a dict and print it
 -Build the [year, 'name rank', ... ] list and print it
 -Fix main() to use the extract_names list
"""

def extract_names(filename):
	"""
	Given a file name for baby.html, returns a list starting with the year string
	followed by the name-rank strings in alphabetical order.
	['2006', 'Aaliyah 91', Aaron 57', 'Abagail 895', ' ...]
	"""
	# +++your code here+++
	result = []
	try:
		html = open(filename, 'r')
		names = {}
		for line in html:
			#the year
			match_title = re.search(r'<h3 align="center">Popularity\sin\s(\d\d\d\d)</h3>', line, re.I)
			if match_title:
				year = match_title.group(1)
				result.append(year)
			#the names
			names_tuples = re.findall(r'<tr align="right"><td>(\d+)</td><td>(\w+)</td><td>(\w+)</td>', line)
			for rank, boy, girl in names_tuples:
				if boy not in names:
					names[boy] = rank
				if girl not in names:
					names[girl] = rank
		#sort the dic names
		names_sorted = sorted(names.keys())
		for name in names_sorted:
			result.append(name + ',' + names[name])
		
	except :
		print 'The file %s could not be readed' % filename
	return result

def main():
	# This command-line parsing code is provided.
	# Make a list of command line arguments, omitting the [0] element
	# which is the script itself.
	args = sys.argv[1:]

	if not args:
		print 'usage: [--summaryfile] file [file ...]'
		sys.exit(1)

	# Notice the summary flag and remove it from args if it is present.
	summary = False
	if args[0] == '--summaryfile':
		summary = True
		del args[0]

	# +++your code here+++
	# For each filename, get the names, then either print the text output
	# or write it to a summary file
	filename = args[0]
	names = extract_names(filename)
	result = '\n'.join(names)
	if summary:
		S = filename.split('.')
		file = open('csv/' + S[0] + '_summary.csv', 'w')
		file.write(result)
		file.close()
		print 'file ' + S[0] + '_summary.csv was created'
	else:
		print result

if __name__ == '__main__':
	main()
