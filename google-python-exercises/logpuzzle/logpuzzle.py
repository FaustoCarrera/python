#!/usr/bin/python
# Copyright 2010 Google Inc.
# Licensed under the Apache License, Version 2.0
# http://www.apache.org/licenses/LICENSE-2.0

# Google's Python Class
# http://code.google.com/edu/languages/google-python-class/

import os
import re
import sys
import urllib

"""Logpuzzle exercise
Given an apache logfile, find the puzzle urls and download the images.

Here's what a puzzle url looks like:
10.254.254.28 - - [06/Aug/2007:00:13:48 -0700] "GET /~foo/puzzle-bar-aaab.jpg HTTP/1.0" 302 528 "-" "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6"
"""

def url_sort_key(url):
	"""Used to order the urls in increasing order by 2nd word if present."""
	match = re.search(r'-(\w+)-(\w+)\.\w+', url)
	if match:
		return match.group(2)
	else:
		return url

def read_urls(filename):
	"""Returns a list of the puzzle urls from the given log file,
	extracting the hostname from the filename itself.
	Screens out duplicate urls and returns the urls sorted into
	increasing order."""
	# +++your code here+++
	underbar = filename.index('_')
	host = filename[underbar + 1:]
	result = {}
	try:
		log = open(filename, 'r')
		for line in log:
			match = re.search(r'"GET (\S+)', line)
			if match:
				image_path = match.group(1)
				if 'puzzle' in image_path:
					result['http://' + host + image_path] = 1
		log.close()
	except:
		print 'the file %s cannot be read' % filename
	return sorted(result.keys(), key=url_sort_key)
	

def download_images(img_urls, dir):
	"""Given the urls already in the correct order, downloads
	each image into the given directory.
	Gives the images local filenames img0, img1, and so on.
	Creates an index.html in the directory
	with an img tag to show each local image file.
	Creates the directory if necessary.
	"""
	# +++your code here+++
	count = 0
	# create the dir
	if not os.path.exists(dir):
		os.makedirs(dir)
	#download the files
	total = len(img_urls)
	percent = 100.0 / total
	print 'This puzzle have %d files...' % total
	html = os.path.join(dir, 'index.html')
	index = open(html, 'w');
	for img_url in img_urls:
		image = img_url.split('.')
		image_name = 'img%d.%s' % (count, image[-1])
		destin_image = os.path.abspath(os.path.join(dir, image_name))
		urllib.urlretrieve(img_url, destin_image)
		image_html = '<img src="%s" />' % image_name
		index.write(image_html)
		current_percent = ((count + 1) * percent) * 1.0
		print 'downloaded file %d ---- %s%s' % (count, current_percent, '%')
		count+=1
	index.close()
	
def main():
	args = sys.argv[1:]

	if not args:
		print 'usage: [--todir dir] logfile '
		sys.exit(1)

	todir = ''
	if args[0] == '--todir':
		todir = args[1]
		del args[0:2]

	img_urls = read_urls(args[0])

	if todir:
		download_images(img_urls, todir)
	else:
		print '\n'.join(img_urls)

if __name__ == '__main__':
	main()
