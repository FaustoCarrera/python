#!/usr/bin/env python
#-*- coding: utf-8 -*-

numbers = [0, 1, 2, 3, 4, 5]
data = [
{"name": "jhon doe", "height": 1.80},
{"name": "jhon smith", "height": 1.60},
{"name": "Sam"}
]
# map
# apply the function to each item and return the item
sqr = map(lambda x: x * x, numbers)
print "square: %s" % sqr

# reduce
# aplly the function and combine the items
sum = reduce(lambda a, x: a + x, numbers)
print "sum: %s" % sum

# filter
# takes two arguments, a function and a list, and returns a list
heights = map(lambda x: x["height"], filter(lambda x: 'height' in x, data))
print "heights: %s" % heights

if len(heights) > 0:
    avg = reduce(lambda a, x: a + x, heights) / len(heights)
    print "average height: %s" % avg