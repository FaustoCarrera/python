#!/usr/bin/env python

def sequential(stack, needle):
    loop = 0
    total = len(stack)
    for item in stack:
        loop += 1
        if item == needle:
            print "loops: %s of %s" % (loop, total)
            return True
    print "loops: %s of %s" % (loop, total)
    return False

def ordered(stack, needle):
    loop = 0
    total = len(stack)
    for item in stack:
        loop += 1
        if item == needle:
            print "loops: %s of %s" % (loop, total)
            return True
        if item > needle:
            print "loops: %s of %s" % (loop, total)
            return False
    print "loops: %s of %s" % (loop, total)
    return False

if __name__ == "__main__":
    print sequential(range(100), 4)
    print sequential(range(100), 12)
    print ordered([0, 1, 2, 3, 5, 6, 7, 8, 9, 10], 4)
    print ordered(range(10), 12)