#!/usr/bin/env python

def search(stack, needle):
    loop = 0
    total = len(stack)
    first = 0
    last = len(stack) - 1
    found = False
    while first <= last and not found:
        loop += 1
        middle = (first + last) // 2
        if needle == stack[middle]:
            print "loops: %s of %s" % (loop, total)
            return True
        else:
            if needle < stack[middle]:
                last = middle - 1
            elif needle > stack[middle]:
                first = middle + 1
    print "loops: %s of %s" % (loop, total)
    return found

if __name__ == "__main__":
    print search(range(20000), 56)