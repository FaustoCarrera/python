#!/usr/bin/env python


def sum_numbers(num):
    if len(num) == 1:
        return num[0]
    else:
        return num[0] + sum_numbers(num[1:])

if __name__ == "__main__":
    print sum_numbers([x for x in range(155)])
