#!/usr/bin/env python


def changeBase(n, base):
    values = "0123456789ABCDEF"
    if n < base:
        return values[n]
    else:
        return changeBase(n // base, base) + values[n % base]

if __name__ == "__main__":
    print changeBase(101, 6)
