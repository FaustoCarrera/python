#!/usr/bin/env python
#      1
#     1 1
#    1 2 1
#   1 3 3 1
#  1 4 6 4 1
#1 5 10 10 5 1


def pascal(rows):
    for rownum in range(rows):
        newValue = 1
        PrintingList = [newValue]
        for iteration in range(rownum):
            newValue = newValue * (rownum - iteration) / (iteration + 1)
            PrintingList.append(int(newValue))
        print PrintingList

if __name__ == "__main__":
    pascal(9)
