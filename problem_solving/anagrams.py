#!/usr/bin/env python

def anagrams(a, b):
    match = False
    if len(a) == len(b):
        if sorted(a) == sorted(b):
            match = True
    return match

print anagrams("car", "arc")
print anagrams("lorem", "ipsum")
print anagrams("dolor", "sit_amet")
print anagrams("python", "typhon")
print anagrams("earth", "heart")
