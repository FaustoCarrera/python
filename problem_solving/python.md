##Python##

**Algorithm**

a step-by-step list of instructions for solving any instance of the problem that might arise.
Algorithms are finite processes that if followed will solve the problem. Algorithms are solutions.

**Procedural abstraction**

we do not necessarily know how the square root is being calculated, but we know what the function is called and how to use it.
we know that someone implemented a solution to the square root problem but we only need to know how to use it.
this is sometimes referred to as a "black box" view of a process.

**Programming**

is the process of taking an algorithm and encoding it into a notation, a programming language, so that it can be executed by a computer.

**Encapsulation around the data**

The idea is that by encapsulating the details of the implementation, we are hiding them from the user´s view. This is called information hiding.

**Logical perspective**

users point of view

**Physical perspective**

developer point of view

**Abstract data type**

Sometimes abbreviated ADT, is a logical description of how we view the data and the operations that are allowed without regard to how they will be implemented.
This means that we are concerned only with what the data is representing and not with how it will eventually be constructed.

**List**

l = [0,1,2,3,4,5,'a','b']

Method Name|Use                  |Explanation
-----------|---------------------|-----------
append     |alist.append(item)   |Adds a new item to the end of a list
insert     |alist.insert(i,item) |Inserts an item at the ith position in a list
pop        |alist.pop()          |Removes and returns the last item in a list
pop        |alist.pop(i)         |Removes and returns the ith item in a list
sort       |alist.sort()         |Modifies a list to be sorted
reverse    |alist.reverse()      |Modifies a list to be in reverse order
del        |del alist[i]         |Deletes the item in the ith position
index      |alist.index(item)    |Returns the index of the first occurrence of item
count      |alist.count(item)    |Returns the number of occurrences of item
remove     |alist.remove(item)   |Removes the first occurrence of item

**Set**

s = {False, 4.5, 3, 6, 'cat'}

Method Name      |Use                        |Explanation
-----------------|---------------------------|-----------
union            |aset.union(otherset)       |Returns a new set with all elements from both sets
intersection     |aset.intersection(otherset)|Returns a new set with only those elements common to both sets
difference       |aset.difference(otherset)  |Returns a new set with all items from first set not in second
issubset         |aset.issubset(otherset)    |Asks whether all elements of one set are in the other
add              |aset.add(item)             |Adds item to the set
remove           |aset.remove(item)          |Removes item from the set
pop              |aset.pop()                 |Removes an arbitrary element from the set
clear            |aset.clear()               |Removes all elements from the set

**Dictionary**

d = {"brad": 1137, "david": 1410}

Operator    |Use              |Explanation
------------|-----------------|----------
[]          |myDict[k]        |Returns the value associated with k, otherwise its an error
in          |key in adict     |Returns True if key is in the dictionary, False otherwise
del         |del adict[key]   |Removes the entry from the dictionary

Method Name    |Use               |Explanation
---------------|------------------|-------------
keys           |adict.keys()      |Returns the keys of the dictionary in a dict_keys object
values         |adict.values()    |Returns the values of the dictionary in a dict_values object
items          |adict.items()     |Returns the key-value pairs in a dict_items object
get            |adict.get(k)      |Returns the value associated with k, None otherwise
get            |adict.get(k,alt)  |Returns the value associated with k, alt otherwise

**String**

s = "string"

Method name | Use                   | Explanation
------------|-----------------------|------------
center      |astring.center(w)      |Returns a string centered in a field of size w
count       |astring.count(item)    |Returns the number of occurrences of item in the string
ljust       |astring.ljust(w)       |Returns a string left-justified in a field of size w
lower       |astring.lower()        |Returns a string in all lowercase
rjust       |astring.rjust(w)       |Returns a string right-justified in a field of size w
find        |astring.find(item)     |Returns the index of the first occurrence of item
split       |astring.split(schar)   |Splits a string into substrings at schar

**String formatting**

Character|Output Format
---------|-------------
d, i     |Integer
u        |Unsigned integer
f        |Floating point as m.ddddd
e        |Floating point as m.ddddde+/-xx
E        |Floating point as m.dddddE+/-xx
g        |Use %e for exponents less than −4 or greater than +5, otherwise use %f
c        |Single character
s        |String, or any Python data object that can be converted to a string by using the str function.
%        |Insert a literal % character

Modifier |Example   |Description
---------|----------|----------------
number   |%20d      |Put the value in a field width of 20
-        |%-20d     |Put the value in a field 20 characters wide, left-justified
+        |%+20d     |Put the value in a field 20 characters wide, right-justified
0        |%020d     |Put the value in a field 20 characters wide, fill in with leading zeros.
.        |%20.2f    |Put the value in a field 20 characters wide with 2 characters to the right of the decimal point.
(name)   |%(name)d  |Get the value from the supplied dictionary using name as the key.

##Algorithm Analysis##

an algorithm is a generic, step-by-step list of instructions for solving a problem.
It is a method for solving any instance of the problem such that given a particular input, the algorithm produces the desired result.
A program, on the other hand, is an algorithm that has been encoded into some programming language.
There may be many programs for the same algorithm, depending on the programmer and the programming language being used.
Consider the amount of space or memory an algorithm requires to solve the problem.
Analyze and compare algorithms based on the amount of time they require to execute, "execution time".

<pre>
   import time
   start = time.time()
   ...
   end = time.time()
   exec_time = end - start # seconds
</pre>

##Big-O Notation (order of magnitude)##

algorithm's efficiency in terms of execution time, independent of any particular program or computer, it is important to quantify the number of operations or steps that the algorithm will require.

n = the size of the problem
O(f(n))

f(n)     |Name
---------|-------
1        |Constant
log n    |Logarithmic
n        |Linear
n logn   |Log Linear
n^2      |Quadratic
n^3      |Cubic
2^n      |Exponential

![Big-o functions](newplot.png "Plot of Common Big-O Functions")

<pre>
a=5
b=6
c=10
for i in range(n):
   for j in range(n):
      x = i * i
      y = j * j
      z = i * j
for k in range(n):
   w = a*k + 45
   v = b*b
d = 33
</pre>

T(n) = 3 + 3n^2 + 2n +1 = 3n^2 + 2n +4

O(n^2)

**Self Check**

Write two Python functions to find the minimum number in a list. The first function should compare each number to every other number on the list. O(n^2). The second function should be linear O(n).

min_number.py

Write a boolean function that will take two strings and return whether they are anagrams. One string is an anagram of another if the second is simply a rearrangement of the first. For example, 'heart' and 'earth' are anagrams. The strings 'python' and 'typhon' are anagrams as well. For the sake of simplicity, we will assume that the two strings in question are of equal length and that they are made up of symbols from the set of 26 lowercase alphabetic characters.

anagrams.py

**Big-O Efficiency of Python List Operators**

n: number of loops

k: the size of the list that is being

operation         | Big-O Efficiency
------------------|-----------
index []          |O(1)
index assignment  |O(1)
append            |O(1)
pop()             |O(1)
pop(i)            |O(n)
insert(i,item)    |O(n)
del operator      |O(n)
iteration         |O(n)
contains (in)     |O(n)
get slice [x:y]   |O(k)
del slice         |O(n)
set slice         |O(n+k)
reverse           |O(n)
concatenate       |O(k)
sort              |O(n log n)
multiply          |O(nk)

timing.py

**Big-O Efficiency of Python Dictionary Operations**

operation      | Big-O Efficiency
---------------|------------------
copy           |O(n)
get item       |O(1)
set item       |O(1)
delete item    |O(1)
contains (in)  |O(1)
iteration      |O(n)

![List vs Dictionary](listvdict.png "Comparing the in Operator for Python Lists and Dictionaries")

**Exercises*

* Devise an experiment to verify that the list index operator is O(1) // bigo/exercise1.py
* Devise an experiment to verify that get item and set item are O(1) for dictionaries.
* Devise an experiment that compares the performance of the del operator on lists and dictionaries.
* Given a list of numbers in random order write a linear time algorithm to find the kth smallest number in the list. Explain why your algorithm is linear.
* Can you improve the algorithm from the previous problem to be O(nlog(n))?


##Basic Data Structures##

**Linear Structures**

Stacks, queues, deques, and lists are examples of data collections whose items are ordered depending on how they are added or removed.
Once an item is added, it stays in that position relative to the other elements that came before and came after it.
Collections such as these are often referred to as linear data structures.

Linear structures can be thought of as having two ends. Sometimes these ends are referred to as the "left" and the "right" or in some cases the "front" and the "rear."
You could also call them the "top" and the "bottom." The names given to the ends are not significant.
What distinguishes one linear structure from another is the way in which items are added and removed, in particular the location where these additions and removals occur.
For example, a structure might allow new items to be added at only one end. Some structures might allow items to be removed from either end.

**Stack**

A stack (sometimes called a "push-down stack") is an ordered collection of items where the addition of new items and the removal of existing items always takes place at the same end.
This end is commonly referred to as the "top"

The base of the stack is significant since items stored in the stack that are closer to the base represent those that have been in the stack the longest.
The most recently added item is the one that is in position to be removed first.
This ordering principle is sometimes called LIFO, last-in first-out.
It provides an ordering based on length of time in the collection.
Newer items are near the top, while older items are near the base.

**The Stack Abstract Data Type**

The stack abstract data type is defined by the following structure and operations. A stack is structured, as described above, as an ordered collection of items where items are added to and removed from the end called the "top." Stacks are ordered LIFO. The stack operations are given below.

* Stack() creates a new stack that is empty. It needs no parameters and returns an empty stack.
* push(item) adds a new item to the top of the stack. It needs the item and returns nothing.
* pop() removes the top item from the stack. It needs no parameters and returns the item. The stack is modified.
* peek() returns the top item from the stack but does not remove it. It needs no parameters. The stack is not modified.
* isEmpty() tests to see whether the stack is empty. It needs no parameters and returns a boolean value.
* size() returns the number of items on the stack. It needs no parameters and returns an integer.

datastructures/stack/stack.py

* Write a function revstring(mystr) that uses a stack to reverse the characters in a string.

datastructures/stack/string_reverse.py

**The Queue Abstract Data Type**

A queue is an ordered collection of items where the addition of new items happens at one end, called the "rear" and the removal of existing items occurs at the other end, commonly called the "front." As an element enters the queue it starts at the rear and makes its way toward the front, waiting until that time when it is the next element to be removed.

The most recently added item in the queue must wait at the end of the collection. The item that has been in the collection the longest is at the front. This ordering principle is sometimes called FIFO, first-in first-out. It is also known as "first-come first-served."

datastructures/queues/queue.py

**The Deque Abstract Data Type**

A deque, also known as a double-ended queue, is an ordered collection of items similar to the queue. It has two ends, a front and a rear, and the items remain positioned in the collection. What makes a deque different is the unrestrictive nature of adding and removing items. New items can be added at either the front or the rear. Likewise, existing items can be removed from either end. In a sense, this hybrid linear structure provides all the capabilities of stacks and queues in a single data structure.

##Recursion##

Recursion is a method of solving problems that involves breaking a problem down into smaller and smaller subproblems until you get to a small enough problem that it can be solved trivially.

Usually recursion involves a function calling itself. While it may not seem like much on the surface, recursion allows us to write elegant solutions to problems that may otherwise be very difficult to program.

**The Three Laws of Recursion**

* A recursive algorithm must have a base case.
* A recursive algorithm must change its state and move toward the base case.
* A recursive algorithm must call itself, recursively.

A base case is the condition that allows the algorithm to stop recursing. A base case is typically a problem that is small enough to solve directly.

A change of state means that some data that the algorithm is using is modified. Usually the data that represents our problem gets smaller in some way.

The final law is that the algorithm must call itself. This is the very definition of recursion.

recursion/base.py
recursion/factorial.py
recursion/reverse.py
recursion/sum.py
recursion/pascal.py

##Sorting and Searching##

**sequential search**

* Starting at the first item in the list, we simply move from item to item, following the underlying sequential ordering until we either find what we are looking for or run out of items.

search/sequential.py

**binary search**

* Instead of searching the list in sequence, a binary search will start by examining the middle item.

search/binary.py

**hashing**

* A hash table is a collection of items which are stored in such a way as to make it easy to find them later. 

##Sorting##

* Sorting is the process of placing elements from a collection in some kind of order.
* The efficiency of a sorting algorithm is related to the number of items being processed.

**The Bubble Sort**

* The bubble sort makes multiple passes through a list.
* It compares adjacent items and exchanges those that are out of order.
* Each pass through the list places the next largest value in its proper place.
* In essence, each item "bubbles" up to the location where it belongs.

