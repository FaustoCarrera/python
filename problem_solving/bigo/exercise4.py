#!/usr/bin/env python

import time
from random import randrange

l = [randrange(10, 10000) for x in range(10000)]

def getMinA(alist):
    overal_min = alist[0]
    for i in alist:
        is_smallest = True
        for j in alist:
            if i > j:
                is_smallest = False
        if is_smallest:
            overal_min = i
    return overal_min

def getMinB(alist):
    overal_min = alist[0]
    for i in alist:
        if i < overal_min:
            overal_min = i
    return overal_min

start = time.time()
print getMinA(l)
end = time.time()
print end - start

start = time.time()
print getMinB(l)
end = time.time()
print end - start