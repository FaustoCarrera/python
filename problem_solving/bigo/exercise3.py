#!/usr/bin/env python
# Devise an experiment that compares the performance of the del operator on lists and dictionaries.

from timeit import Timer


for i in range(10000, 100000, 200):
    x = list(range(i))
    x.reverse()
    t = Timer("for k in x: del x[k]", "from __main__ import x")
    lst_time = t.timeit(number=1)

    x = {}
    for j in range(i):
        x[j] = None
    x_k = x.keys()
    u = Timer("for k in x_k: del x[k]", "from __main__ import x, x_k")
    dct_time = u.timeit(number=1)
    print("%d,%5.3f,%5.3f" % (i, lst_time, dct_time))
