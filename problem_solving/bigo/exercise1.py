#!/usr/bin/env python
# Devise an experiment to verify that the list index operator is O(1)

from timeit import Timer
import random

x = [j for j in range(25000)]

for i in range(100):
    r = random.randrange(25000)
    t = Timer("x[%d]" % i, "from __main__ import x")
    idx_time = t.timeit(number=1000)
    print("%d,%5.d,%5.3f" % (i, r, idx_time))
