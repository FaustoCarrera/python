#!/usr/bin/env python
# Devise an experiment to verify that get item and set item are O(1) for dictionaries.

from timeit import Timer
import random

x = {}
for j in range(25000):
    x[j] = None

for i in range(100):
    r = random.randrange(25000)
    t = Timer("x[%d]" % r, "from __main__ import x")
    get_time = t.timeit(number=1000)
    u = Timer("x[%d] = 0" % r, "from __main__ import x")
    set_time = u.timeit(number=1000)
    print("%d,%5.d,%5.3f,%5.3f" % (i, r, get_time, set_time))
