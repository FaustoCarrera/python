#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
Module for making a deque
new items can be added at either the front or the rear, existing items can be removed from either end
"""

class Deque(object):

    """
    Generates a new deque
    """

    def __init__(self):
        self.items = []

    def addFront(self, item):
        """Add an item to the front of the deque"""
        self.items.insert(0, item)

    def addRear(self, item):
        """Add an item to the back of the deque"""
        self.items.append(item)

    def removeFront(self):
        """Remove an item from the front of the deque"""
        return self.items.pop(0)

    def removeRear(self):
        """Remove an item from the back of the deque"""
        return self.items.pop()

    def isEmpty(self):
        return self.items == []

    def size(self):
        return len(self.items)

if __name__ == "__main__":
    d = Deque()
    print d.isEmpty() # True
    d.addRear(4)
    d.addRear('dog')
    d.addFront('cat')
    d.addFront(True)
    print d.size() # 4
    print d.isEmpty() # False
    d.addRear(8.4)
    print d.removeRear() # 8.4
    print d.removeFront() # True