#!/usr/bin/env python
#-*- coding: utf-8 -*-

from deque import Deque
import unicodedata

def palindrom(phrase):
    is_palindrom = True
    # remove the odd characters
    phrase = unicodedata.normalize("NFD", phrase).encode("ascii", "ignore")
    phrase = phrase.replace(",", "").replace(".", "").replace(":", "").replace(" ", "").lower().strip()
    # new deque
    d = Deque()
    # add chars to deque
    for char in phrase:
        d.addRear(char)
    # check if we have a palindrom
    while d.size() > 1:
        f = d.removeFront()
        r = d.removeRear()
        if f != r:
            is_palindrom = False
            break
    return is_palindrom


if __name__ == "__main__":
    phrases = [
        u"añora la roña",
        u"acaso hubo búhos acá",
        u"se van sus naves",
        u"adivina ya te opina, ya ni miles origina, ya ni cetro me domina, ya ni monarcas, a repaso ni mulato carreta, acaso nicotina, ya ni cita vecino, anima cocina, pedazo gallina, cedazo terso nos retoza de canilla goza, de pánico camina, ónice vaticina, ya ni tocino saca, a terracota luminosa pera, sacra nómina y ánimo de mortecina, ya ni giros elimina, ya ni poeta, ya ni vida",
        u"allí por la tropa portado, traído a ese paraje de maniobras, una tipa como capitán usar boina me dejara, pese a odiar toda tropa por tal ropilla",
        u"allí si maría avisa y así va a ir a mi silla",
        u"átale, demoníaco caín, o me delata",
        u"ateo por arabia iba raro poeta",
        u"dábale arroz a la zorra el abad",
        u"la ruta nos aportó otro paso natural",
        u"las nemocón no comen sal",
        u"nada, yo soy adán",
        u"no di mi decoro, cedí mi don",
        u"no lata, no: la totalidad arada dilato talón a talón",
        u"a la catalana banal, atácala",
        u"Lorem ipsum dolor sit amet"
    ]
    for p in phrases:
        print palindrom(p)