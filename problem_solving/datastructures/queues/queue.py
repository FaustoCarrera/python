#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
Module for making a queue
Queues maintain a FIFO ordering property
"""

class Queue(object):

    """
    Generates a new queue
    """

    def __init__(self):
        self.items = []

    def enqueue(self, item):
        """Add an item to the queue"""
        self.items.append(item)

    def dequeue(self):
        """Remove an item to the queue"""
        return self.items.pop(0)

    def isEmpty(self):
        """Check if the queue is empty"""
        return self.items == []

    def size(self):
        """Check the size of the queue"""
        return len(self.items)

if __name__ == "__main__":
    q = Queue()
    print q.isEmpty() # True
    q.enqueue(4) # [4]
    q.enqueue('dog') # ['dog',4]
    q.enqueue(True) # [True,'dog',4]
    print q.size() # 3
    print q.isEmpty() # False
    q.enqueue(8.4) #  [8.4,True,'dog',4]
    print q.dequeue() # 4
    print q.dequeue() # dog
    print q.size() # 2