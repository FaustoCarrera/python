#!/usr/bin/env python
#-*- coding: utf-8 -*-

from queue import Queue

def hot_potato(listnames, num):
    q = Queue()
    # add childs
    for name in listnames:
        q.enqueue(name)

    while q.size() > 1: # check if we have childs
        for i in range(num): # loop the num times
            child = q.dequeue()
            q.enqueue(child)
        q.dequeue() # remove the first child in the row
    return q.dequeue()

if __name__ == "__main__":
    print hot_potato(["Bill","David","Susan","Jane","Kent","Brad"], 7)