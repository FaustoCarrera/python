#!/usr/bin/env python
#-*- coding: utf-8 -*-

import stack

def changeBase(number, base):
    s = stack.Stack()
    digits = "0123456789ABCDEF"
    while number > 0:
        r = number % base
        s.push(r)
        number //= base
    res = ""
    while not s.isEmpty():
        res += digits[s.pop()]
    return res

if __name__ == "__main__":
    print changeBase(101, 2) # binary
    print changeBase(101, 4) #
    print changeBase(101, 7) #
    print changeBase(101, 8) # octal
    print changeBase(101, 16) # hex
