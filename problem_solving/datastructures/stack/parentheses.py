#!/usr/bin/env python
#-*- coding: utf-8 -*-

import stack

def parChecker(par):
    s = stack.Stack()
    i = 0
    balanced = True
    while i < len(par) and balanced:
        char = par[i]
        if char in "({[":
            s.push(char)
        else:
            if s.isEmpty():
                balanced = False
            else:
                symbol = s.pop()
                if not matches(symbol, char):
                    balanced = False
        i += 1
    if s.isEmpty() and balanced:
        return True
    else:
        return False

def matches(open, close):
    opener = "({["
    closer = ")}]"
    return opener.index(open) == closer.index(close)

if __name__ == "__main__":
    print parChecker("((()()))")
    print parChecker("((())")
    print parChecker("{[()]()()[()]}")