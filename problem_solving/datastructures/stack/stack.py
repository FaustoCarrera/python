#!/usr/bin/env python
#-*- codig: utf-8 -*-
"""
Module for making a stack
Stack maintains the LIFO ordering property
"""

class Stack(object):

    """
    Generates a new stack
    """

    def __init__(self):
        self.items = []

    def push(self, item):
        """Add an item to the stack"""
        self.items.append(item)

    def pop(self):
        """get the last item of the stack and modify the stack"""
        return self.items.pop()

    def peek(self):
        """get the last item of the stack and keep the stack unmodified"""
        last_index = len(self.items) - 1
        return self.items[last_index]

    def size(self):
        """get the size of the stack"""
        return len(self.items)

    def isEmpty(self):
        """check if the stack is empty"""
        return not(bool(len(self.items)))

if __name__ == "__main__":
    s = Stack()
    print("-=-= is empty =-=-")
    print(s.isEmpty())
    print("-=-= adding items (2) =-=-")
    s.push(4)
    s.push('dog')
    print("-=-= peek item =-=-")
    print(s.peek())
    print("-=-= adding items (1) =-=-")
    s.push(True)
    print("-=-= get the size =-=-")
    print(s.size())
    print("-=-= is empty =-=-")
    print(s.isEmpty())
    print("-=-= adding items (1) =-=-")
    s.push(8.4)
    print("-=-= pop items (2) =-=-")
    print(s.pop())
    print(s.pop())
    print("-=-= get the size =-=-")
    print(s.size())