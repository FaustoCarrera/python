#!/usr/bin/env python
#-*- codig: utf-8 -*-
# Write a function revstring(mystr) that uses a stack to reverse the characters in a string.

import stack


def revstring(mystr):
    r = ""
    s = stack.Stack()
    for char in mystr:
        s.push(char)
    while not s.isEmpty():
        r += s.pop()
    return r

if __name__ == "__main__":
    my_str = "apple"
    reverse = "elppa"
    if reverse == revstring(my_str):
        print "ok"
    else:
        print "nope"