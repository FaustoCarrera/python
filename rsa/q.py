#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys

def getQ(n, mod):
    n = int(n)
    i = n + 1
    run = True
    while run:
        if is_prime(i):
            q = int(n * i) % int(mod)
            #print "%u : %u" % (i, q)
            if q == 1:
                return i
        i += 1

def is_prime(n):
    """
    returns: true if n prime, else false
    """
    n *= 1.0
    for divisor in range(2, int(n**0.5) + 1):
        if n / divisor == int(n / divisor):
            return False
    return True

if __name__ == "__main__":
    try:
        P = sys.argv[1]
    except:
        P = 100
    try:
        mod = sys.argv[2]
    except:
        mod = 12
    # get Q
    Q = getQ(P, mod)
    print "P: %s && Q: %s" % (P, Q)