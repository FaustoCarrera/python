#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys

def getY(x, mod):
    x = int(x)
    mod = int(mod)
    i = 1
    run = True
    while run:
        # print "%s %s" % (i, mod)
        if (i % mod) == x:
            return i
        i += 1

if __name__ == "__main__":
    try:
        x = sys.argv[1]
    except:
        x = 1
    try:
        mod = sys.argv[2]
    except:
        mod = 1
    # get Y
    y = getY(x, mod)
    print "y: %s && mod: %s" % (y, mod)