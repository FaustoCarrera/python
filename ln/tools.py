#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import ConfigParser


class Tools(object):

    def __init__(self):
        pass

    def arguments(self, raw_args):
        args = {}
        raw_args = raw_args[1:]
        for arg in raw_args:
            if "--" in arg:
                parts = arg.split("=")
                key = parts[0].replace("--", "")
                args[key] = parts[1]
            elif "-" in arg:
                key = arg.replace("-", "")
                args[key] = True
        return args

    def getDefaults(self, args, key, default):
        try:
            value = args[key]
        except:
            value = default
        return value

    def isEmail(self, email):
        if re.match("[^@]+@[^@]+\.[^@]+", email):
            return True
        else:
            return False

    def getConfig(self, config_file):
        result = {}
        # get config file
        configParser = ConfigParser.RawConfigParser()
        configFilePath = config_file
        configParser.read(configFilePath)
        # database
        result["database"] = {
            "host": configParser.get("database", "host"),
            "user": configParser.get("database", "user"),
            "password": configParser.get("database", "password"),
            "database": configParser.get("database", "db")
        }
        # files sources
        result["files"] = {
            "demo": configParser.get("files", "demo_path"),
            "source": configParser.get("files", "source_path"),
            "destination": configParser.get("files", "destin_path"),
            "backup": configParser.get("files", "backup_path")
        }
        return result

if __name__ == "__main__":
    tools = Tools()
    print(tools.getConfig('app.conf'))
