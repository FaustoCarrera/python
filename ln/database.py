#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import tools
import mysql.connector
from mysql.connector import errorcode


class DataBase(object):
    config = {}
    connection = False

    def __init__(self, config):
        self.configure(config)

    def configure(self, config):
        self.config = {
            "host": config["host"],
            "user": config["user"],
            "password": config["password"],
            "database": config["database"]
        }

    def connect(self):
        try:
            config = self.config
            self.connection = mysql.connector.connect(**config)
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                sys.exit("Error wrong username or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                sys.exit("Database does not exists")
            else:
                sys.exit(err)

    def saveEmail(self, email, domain, timestamp):
        q = "INSERT INTO emails VALUES (NULL, '{0}', '{1}', '{2}');"
        cursor = self.connection.cursor()
        query = q.format(email, domain, timestamp)
        cursor.execute(query)
        uid = cursor.lastrowid
        cursor.close()
        return uid

    def saveEmail_(self, email, domain, timestamp):
        q = "INSERT INTO emails VALUES (NULL, '{0}', '{1}', '{2}');"
        if not self.checkEmail(email):
            cursor = self.connection.cursor()
            query = q.format(email, domain, timestamp)
            cursor.execute(query)
            uid = cursor.lastrowid
            cursor.close()
            return uid
        else:
            self.log(email)
            return 0

    def checkEmail(self, email):
        q = "SELECT id FROM emails WHERE email = '{0}'"
        query = q.format(email)
        cursor = self.connection.cursor()
        cursor.execute(query)
        row = cursor.fetchone()
        try:
            return row[0]
        except:
            return 0

    def close(self):
        self.connection.close()

    def commit(self):
        self.connection.commit()

    def log(self, email):
        message = "%s already exists\n" % email
        d = open("log", "a")
        d.write(message)

if __name__ == "__main__":
    print "database"
    tools = tools.Tools()
    full_config = tools.getConfig('app.conf')
    config = full_config["database"]
    db = DataBase(config)
    db.connect()
    # save email
    uid = db.saveEmail("nobody@nowhere.com", "nowhere.com", 1398477600)
    print uid
    # check email
    uid = db.checkEmail("nobody@nowhere.com.ar")
    print uid
    db.close()
