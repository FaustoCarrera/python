#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import time
import tools
from database import DataBase
import mmap
import math
import shutil

config = {}
tools = tools.Tools()
limit = 2000


def run(dest, run_parser):
    files_list = getFiles(run_parser)
    path = getPath(run_parser)
    start_time = time.time()
    print "total files [%s]" % len(files_list)
    print "destination [%s]" % dest
    if not run_parser:
        print "run the demo file"
    # go for the files
    for f in files_list:
        total = 0
        emails = []
        to_parse = path + f
        backup = config["files"]["backup"] + f
        this_line = 0
        total_lines = countLines(to_parse)
        print "-~" * 25
        print "reading file %s" % f
        print "total entries %s" % total_lines
        # progress
        percent = int(math.ceil(total_lines / 50))
        print "progress[                                                  ]",
        print "\b" * 52,
        sys.stdout.flush()
        # read lines
        lines = open(to_parse, "r+")
        for line in lines:
            this_line += 1
            # lines
            l = line.split(",")
            # emails
            try:
                email = l[0]
            except:
                email = "no@nowhere.com"
            # timestamp
            try:
                timestamp = l[8]
            except:
                timestamp = int(time.time())
            if tools.isEmail(email):  # check if it's email
                total += 1
                e = email.split("@")
                domain = e[1]
                emails.append({
                    "email": email,
                    "domain": domain,
                    "timestamp": timestamp
                })
                # progress bar
                if (total % percent) == 0:
                    print "\b-",
                    sys.stdout.flush()
            # check the size of the emails list
            if len(emails) > limit or this_line == total_lines:
                if dest == "file":
                    saveToFile(emails)
                elif dest == "database":
                    saveToDatabase(emails)
                else:
                    print "%s, %s" % (email, timestamp)
                del emails[:]
        print "\b]"
        lines.close()
        # move to backup
        if run_parser:
            shutil.move(to_parse, backup)
    print "-~" * 25
    # time
    elapsed = time.time() - start_time
    print getTime(elapsed)
    print "total entries: %d" % total
    print ""


def getTime(elapsed):
    hours = int(round(elapsed / 3600))
    minutes = int(round(round(elapsed / 60) % 60))
    seconds = int(elapsed % 60)
    return "total time: %sh %smin %ssec" % (
        hours,
        minutes,
        seconds
    )


def saveToFile(emails):
    d = open(config["files"]["destination"], "a")
    for email in emails:
        d.write(email["email"] + "\n")


def saveToDatabase(emails):
    db = DataBase(config["database"])
    db.connect()
    for email in emails:
        db.saveEmail(email["email"], email["domain"], email["timestamp"])
    db.commit()
    db.close()
    time.sleep(0.1)


def countLines(filename):
    f = open(filename, "r+")
    buf = mmap.mmap(f.fileno(), 0)
    lines = 0
    readline = buf.readline
    while readline():
        lines += 1
    return lines


def getFiles(run_parser):
    demo_path = config["files"]["demo"]
    real_path = config["files"]["source"]
    path = demo_path if not run_parser else real_path
    files_list = [f for f in os.listdir(path) if os.path.isfile(path + f)]
    return files_list


def getPath(run_parser):
    demo_path = config["files"]["demo"]
    real_path = config["files"]["source"]
    path = demo_path if not run_parser else real_path
    return path

if __name__ == "__main__":
    args = tools.arguments(sys.argv)
    config = tools.getConfig('app.conf')
    dest = tools.getDefaults(args, "dest", None)
    run_parser = tools.getDefaults(args, "run", False)
    show_help = tools.getDefaults(args, "help", False)
    if show_help:
        print "Usage: python %s --dest=[file, database] -[run]" % sys.argv[0]
        print "Source: %s" % config["files"]["source"]
        print "Destination: %s" % config["files"]["destination"]
    else:
        # run(dest, run_parser)
        try:
            run(dest, run_parser)
        except:
            print "An error occurred..."
