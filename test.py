#!/usr/bin/env python
#-*- coding: utf-8 -*-

i = 0;
total = 20
while i < total:
    print "%u: %u" % (i, 2 ** i)
    i += 1