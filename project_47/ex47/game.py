#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Room(object):
    def __init__(self, name, description):
        self.name = name
        self.description = description
        self.paths = {}

    def go(self, direction):
        return self.paths.get(direction, None)

    def addPaths(self, paths):
        self.paths.update(paths)

if __name__ == "__main__":
    silver = Room("SilverRoom", "this is the silver room")
    print silver.name
    print silver.description
    print silver.paths
