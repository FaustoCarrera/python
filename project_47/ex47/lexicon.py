#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Lexicon
A program to take a string of raw input from a user and return a tuple with
token-word pairings.
Author: Fausto Carrera - faustocarrera.com.ar
Copyrights: GNU GPL
Given the raw input of a user, the program will read it completely,
and while doing so, separates each word in a type of word,
and group it in tuples, then the program will return a list of tuples.
"""


class Lexicon(object):

    def __init__(self):
        self.directions = ["north", "south", "east", "west", "up", "down", "left", "right", "back"]
        self.verbs = ["go", "stop", "kill", "eat"]
        self.stops = ["the", "in", "of", "from", "at", "it", "and"]
        self.nouns = ["door", "bear", "princess", "cabinet", "prince", "enemy", "enemies"]

    def scan(self, w):
        L = []
        words = w.split()
        for word in words:
            # check word in lexicon
            L.append(self.get_tupples(word))
        return L

    def get_tupples(self, word):
        S = {"direction": self.directions, "verb": self.verbs, "stop": self.stops, "noun": self.nouns}
        word = word.replace(",", "").replace(".", "").strip()
        result = None
        for k in S.keys():
            lx = S[k]
            if word in lx:
                result = (k, word)
            else:
                # check if it's a number
                n = self.convert_numbers(word)
                if n is not None:
                    result = ("number", n)
        if result:
            return result
        else:
            return ("error", word)

    def convert_numbers(self, n):
        try:
            return int(n)
        except ValueError:
            return None


"""
ParseError
A program to parse errors
Author: Fausto Carrera - faustocarrera.com.ar
Copyrights: GNU GPL
"""


class ParserError(Exception):

    def __init__(self, e):
        raise Exception(e)

"""
Sentence
A program to turn the tuples list from Lexicon into a nice Sentece object that
has subject, ver and object.
Author: Fausto Carrera - faustocarrera.com.ar
Copyrights: GNU GPL
The program loop throug the list of tuples, match different tuples that we
expect on our Subject Verb Object setup, peek a potential tuple so we can
make some decision and skip the things we don't care about like stop words
"""


class Sentence(object):

    def __init__(self):
        pass

    def sentence(self, subject, verb, obj):
        self.subject = subject[1]
        self.verb = verb[1]
        self.obj = obj[1]
        return "%s %s %s" % (self.subject, self.verb, self.obj)

    def peek(self):
        if self.word_list:
            word = self.word_list[0]
            return word[0]
        else:
            return None

    def match(self, expecting):
        if self.word_list:
            word = self.word_list.pop(0)
            if word[0] == expecting:
                return word
            else:
                return None
        else:
            return None

    def skip(self, word_type):
        new_list = []
        for word in self.word_list:
            if word[0] != word_type:
                new_list.append(word)
        self.word_list = new_list

    # parsers

    def parse_verb(self):
        self.skip("stop")
        if self.peek() == "verb":
            return self.match("verb")
        else:
            raise ParserError("Expected a verb next")

    def parse_object(self):
        self.skip("stop")
        next = self.peek()
        if next == "noun":
            return self.match("noun")
        elif next == "direction":
            return self.match("direction")
        else:
            raise ParserError("Expected noun or direction next")

    def parse_subject(self, subj):
        verb = self.parse_verb()
        obj = self.parse_object()
        return self.sentence(subj, verb, obj)

    def parse_sentence(self, word_list):
        self.word_list = word_list
        self.skip("stop")
        start = self.peek()
        if start == "noun":
            subj = self.match("noun")
            return self.parse_subject(subj)
        elif start == "verb":
            return self.parse_subject(("noun", "player"))
        else:
            raise ParserError("Must start with subject, object or verb, not %s" % start)

if __name__ == "__main__":
    lx = Lexicon()
    st = Sentence()

    word_list = lx.scan("go north and kill the enemies")
    result = st.parse_sentence(word_list)
    print result

    word_list = lx.scan("go north")
    result = st.parse_sentence(word_list)
    print result

    word_list = lx.scan("enemies")
    result = st.parse_sentence(word_list)
    print result

    word_list = lx.scan("kill the enemies")
    result = st.parse_sentence(word_list)
    print result
