#!/usrb/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import *
from ex47.lexicon import *

lx = Lexicon()
st = Sentence()


def test_parse_sentence():
    word_list = lx.scan("go north")
    result = st.parse_sentence(word_list)
    assert_equal(result, "player go north")


def test_bad_sentence():
    word_list = lx.scan("enemies")
    # result = st.parse_sentence(word_list)
    assert_raises(Exception, st.parse_sentence, word_list)
