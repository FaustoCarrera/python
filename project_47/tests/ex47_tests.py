#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import *
from ex47.game import Room


def test_room():
    gold = Room("GoldRoom", "This room has gold in it, you have a door at the north")
    assert_equal(gold.name, "GoldRoom")
    assert_equal(gold.paths, {})


def test_room_paths():
    center = Room("Center", "This is the center room")
    north = Room("North", "This is the north room")
    south = Room("South", "This is the south room")
    center.addPaths({"north": north, "south": south})
    assert_equal(center.go("north"), north)
    assert_equal(center.go("south"), south)


def test_map():
    start = Room("Start", "You can go west")
    west = Room("West", "There are trees here")
    down = Room("Dungueon", "It's dark down here")
    start.addPaths({"west": west, "down": down})
    west.addPaths({"east": start})
    down.addPaths({"up": start})
    assert_equal(start.go("west"), west)
    assert_equal(start.go("west").go("east"), start)
    assert_equal(start.go("down").go("up"), start)
