#!/usr/bin/env python
# -*- coding: utf-8 -*-

from nose.tools import *
from ex47.lexicon import Lexicon

lx = Lexicon()


def test_directions():
    assert_equal(lx.scan("north"), [("direction", "north")])
    result = lx.scan("north south east")
    assert_equal(result, [("direction", "north"), ("direction", "south"), ("direction", "east")])


def test_verbs():
    assert_equal(lx.scan("go"), [('verb', 'go')])
    result = lx.scan("go kill eat")
    assert_equal(result, [('verb', 'go'), ('verb', 'kill'), ('verb', 'eat')])


def test_stops():
    assert_equal(lx.scan("the"), [('stop', 'the')])
    result = lx.scan("the in of")
    assert_equal(result, [('stop', 'the'), ('stop', 'in'), ('stop', 'of')])


def test_nouns():
    assert_equal(lx.scan("bear"), [('noun', 'bear')])
    result = lx.scan("bear princess")
    assert_equal(result, [('noun', 'bear'), ('noun', 'princess')])


def test_numbers():
    assert_equal(lx.scan("1234"), [('number', 1234)])
    result = lx.scan("3 91234")
    assert_equal(result, [('number', 3), ('number', 91234)])


def test_errors():
    assert_equal(lx.scan("ASDFADFASDF"), [('error', 'ASDFADFASDF')])
    result = lx.scan("bear IAS princess")
    assert_equal(result, [('noun', 'bear'), ('error', 'IAS'), ('noun', 'princess')])
