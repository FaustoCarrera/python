#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    "name": "Test case",
    "description": "My project",
    "author": "Fausto Carrera a.k.a. SilverDaemon",
    "author_email": "fausto.carrera.f@gmail.com",
    "url": "http://...",
    "download_url": "http://github.com/faustocarrera/testcase",
    "version": "0.1",
    "install_requires": ["nose"],
    "packages": ["project", "bin"],
    "scripts": []
}

setup(**config)
