# !/usr/bin/env python
# -*- coding: utf-8 -*-
import sys


def main(filename, sort):
    words_list = {}
    total = 0
    try:
        f = open(filename, "r")
    except:
        message = "file %s cannot be opened" % filename
        sys.exit(message)
    for l in f:
        words = l.split(" ")
        for w in words:
            word = w.replace(",", "").replace(".", "").replace("/", "")
            word = word.replace(")", "").replace("(", "").replace("‐", "")
            word = word.replace("?", "").replace("-", "").replace("\\", "")
            word = word.replace("\"", "").replace("*", "").replace("@", "")
            word = word.lower().strip()
            if len(word):
                total += 1
                if word in words_list:
                    words_list[word] = words_list[word] + 1
                else:
                    words_list[word] = 1
    result = analyze(total, words_list, sort)
    format(result)


def analyze(total, list, sort):
    percents = {}
    for w in list.keys():
        percent = (list[w] * 100.0) / total
        if sort == "wd":
            key = w
            percents[key] = "%.2f" % percent
        elif sort == "qt":
            key = "%.2f_%s" % (percent, w)
            percents[key] = w
    return percents


def format(result):
    for key in sorted(result.keys()):
        p = key.split("_")
        print "%s : %s" % (p[0], result[key])

if __name__ == "__main__":
    #remove the script name
    args = sys.argv[1:]
    #check the arguments
    if not len(args):
        print "Usage: python %s [text file] [sort option]" % sys.argv[0]
        print "text_file is the name of the file to read"
        print "sort option could be wd for word sorted"
        print "or qt for quantity sorted"
        sys.exit(1)
    # check the sort
    sort_list = ["wd", "qt"]
    sort = sort_list[0]
    if len(args) > 1:
        if args[1] in sort_list:
            sort = args[1]
    # check the sort
    sort_list = ["wd", "qt"]
    sort = sort_list[0]
    if len(args) > 1:
        if args[1] in sort_list:
            sort = args[1]
    main(args[0], sort)
