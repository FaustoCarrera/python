#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script to manipulate images
http://geographiclib.sourceforge.net/cgi-bin/GeoConvert
http://www.itouchmap.com/latlong.html
"""

from PIL import Image
from PIL import ExifTags

# filename = './2015-04-30_21-32-40_797.jpg'
filename = './superman_red.jpg'

pix = Image.open(filename)

# pix.save('./superman_red.png')
# pix.save('./superman_red.tiff')

print filename, pix.format, pix.mode, pix.size

for k in pix.info.keys():
    print k, ':', pix.info[k]

exif = pix._getexif()

if exif:
    for k, v in exif.items():
        if k == 34853:  # GPS data
            print ExifTags.TAGS[k], v
            for gk, gv in v.items():
                print ExifTags.GPSTAGS[gk], gv
