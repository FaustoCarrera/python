#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Cryptography is 'the practice and study of hiding information.' The word is a 
combination of the Greek kryptos ('hidden') and grápho ('I write').

Steganography means 'covered writing' in Greek;
it is the science of writing hidden messages so that no one besides the sender 
and intended recipient suspects there is a message — security through obscurity.

Libraries:
Python Cryptography Toolkit (pycrypto) is a collection of both secure hash 
functions (such as SHA256 and RIPEMD160), and various encryption algorithms 
(AES, DES, RSA, ElGamal, etc.)

https://www.dlitz.net/software/pycrypto/
https://github.com/dlitz/pycrypto
pip install pycrypto

Stepic provides a Python module and also a command-line interface to hide 
arbitrary data within images. It slightly modifies the colours of pixels in the 
image to store the data.

http://domnit.org/stepic/doc/
sudo aptitude install python-stepic
"""

import sys
import os
import Image
from Crypto.Cipher import AES
import stepic


class Steganography(object):

    encrypt = False
    key = None
    iv = None

    def __init__(self, encrypt=False, key=None, iv=None):
        self.encrypt = encrypt
        self.key = key
        self.iv = iv
        if encrypt and not key:
            sys.exit('The encryptation key is required')
        if encrypt and not iv:
            sys.exit('The encryptation iv is required')

    def _open_image(self, image):
        name, ext = os.path.splitext(image)
        img = Image.open(image)
        return (img, name, ext)

    def _complete(self, message):
        module = len(message) % 16
        if module:
            remaining = 16 - module
            message += ',' * remaining
        return message

    def _get_message(self, message):
        message = message.encode('utf-8')
        if self.encrypt:
            message_list = []
            # encrypt
            crypto = AES.new(self.key, AES.MODE_CBC, self.iv)
            ciphertext = crypto.encrypt(self._complete(message))
            # convert to ord
            for c in ciphertext:
                message_list.append(str(ord(c)))
            text_ord = ','.join(message_list)
            return self._complete(text_ord)
        else:
            return message

    def _decrypt(self, ciphertext):
        message = ''
        message_list = ciphertext.split(',')
        for c in message_list:
            if c:
                message += chr(int(c))
        # decrypt
        crypto = AES.new(self.key, AES.MODE_CBC, self.iv)
        message = crypto.decrypt(message)
        return message

    def encode(self, image, message):
        img = self._open_image(image)
        new_image = img[1] + '_steg.png'
        img_steg = stepic.encode(img[0], self._get_message(message))
        img_steg.save(new_image, 'PNG')
        return new_image

    def decode(self, image):
        img = self._open_image(image)
        img_stegan = stepic.decode(img[0])
        message = img_stegan.decode()
        if self.encrypt:
            return self._decrypt(message)
        else:
            return message

if __name__ == '__main__':
    image = './red.png'
    message = 'This is the virus of life'
    key = 'oRCp10KJjUi4Htdlj2nHsi7uumnTgaFV'
    iv = '6YM6ZI9AOn969ZYa'
    steg = Steganography(True, key, iv)
    # encode
    image_steg = steg.encode(image, message)
    print image_steg
    # decode
    print steg.decode(image_steg)
