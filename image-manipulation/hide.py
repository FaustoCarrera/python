#!/usr/bin/env python
# -*- encode: utf-8 -*-
"""
Hide/read messages hidden on images
Usage:
python hide.py --image ./red.png --message 'this is the virus of life' --action save
python hide.py --image ./red.png --message @message.txt --offset 68 --sep 4 --action save
python hide.py --image ./red_copy.png --action read
python hide.py --image ./red_copy.png --action max_chars
python hide.py --message 'this is the virus of life' --action min_image

structure
first 4 positions: message length, max message length 1024
next 2 positions: offset and separation, max offset 256, max separation 256
next positions: message
"""

from PIL import Image
import sys
import os
import argparse


def convert(original, copy):
    "transform image to tiff"
    source = Image.open(original)
    source.save(copy)


def max_chars(image):
    "calculate the max quantity of chars the image can hold"
    len_w, len_h = image.size
    return (len_w * len_h) - 1


def min_image(message, offset, sep):
    "calculate the min dimension a image should have to hold the message"
    sizes = []
    message_len = len(message) + 4 + 2 + offset + (len(message) * sep)
    for i in range(1, message_len + 1):
        if not message_len % i:
            result = '%dx%dpx' % (i, message_len // i)
            sizes.append(result)
    return (message_len, ' '.join(sizes))


def size_data(message_len):
    result = [0, 0, 0, 0]
    div = message_len / 256
    reminder = message_len % 256
    for i in range(0, div):
        result[i] = 256
    if reminder:
        result[div] = reminder
    return result


def save_message(message, image, copy_name, offset, sep):
    "save the message on the image"
    # image size
    len_w, len_h = image.size
    # verify message length
    image_max_chars = len_w * len_h
    message_len = min_image(message, offset, sep)
    if image_max_chars < message_len[0]:
        return 'Error: message is too long for this image'
    if message_len[0] > 1024:
        return 'Error: message max 1024 chars, now is %d chars' % message_len[0]
    # verify offset
    if offset > 256:
        return 'Error: max offset is 256'
    # verify sep
    if sep > 10:
        return 'Error: max sep is 10'
    # metadata
    metadata = size_data(message_len[0])
    metadata.append(offset)
    metadata.append(sep)
    # message as list
    message_list = []
    # append offset
    for i in range(0, offset):
        message_list.append(None)
    # append message
    for c in message.encode('utf-8'):
        message_list.append(ord(c))
        for i in range(0, sep):
            message_list.append(None)
    # save message
    count = 0
    for pos_x in range(len_h):
        for pos_y in range(len_w):
            # read pixels
            spectrum = image.getpixel((pos_x, pos_y))
            spectrum = list(spectrum)
            if message_list[count]:
                spectrum[0] = message_list[count]
            # save pixels
            image.putpixel((pos_x, pos_y), tuple(spectrum))
            count += 1
            if count >= len(message_list):
                break
        if count >= len(message_list):
            break
    image.save(copy_name)
    return copy_name


def read_message(image):
    "read the message"
    message = []
    # image size
    len_w, len_h = image.size
    # message length
    first_pixel = image.getpixel((0, 0))
    message_len = first_pixel[0]
    # read pixels
    for pos_x in range(len_h):
        for pos_y in range(len_w):
            # read pixels
            rgb = image.getpixel((pos_x, pos_y))
            message.append(chr(rgb[0]))
            message_len -= 1
            if not message_len:
                break
        if not message_len:
            break
    del message[0]
    # message = reversed(message)
    return ''.join(message)


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to hide messages on images')
    parser.add_argument(
        '--image', required=False, type=str, help='image path to be used', default=False)
    parser.add_argument('--message', required=False,
                        type=str, help='message to be encrypted', default=False)
    parser.add_argument('--offset', required=False, type=int,
                        help='position where the message start', default=0)
    parser.add_argument(
        '--sep', required=False, type=int, help='separation between letters', default=0)
    parser.add_argument('--action', required=False, type=str, help='action',
                        default='max_chars', choices=['min_image', 'max_chars', 'save', 'read'])
    parsed_args = parser.parse_args()
    # check message
    if parsed_args.action == 'save' and not parsed_args.message:
        parser.error('Message is missing')
    if parsed_args.action == 'min_image' and not parsed_args.message:
        parser.error('Message is missing')
    # check image
    if parsed_args.action == 'save' and not parsed_args.image:
        parser.error('Image is missing')
    if parsed_args.action == 'max_chars' and not parsed_args.image:
        parser.error('Message is missing')
    if parsed_args.action == 'read' and not parsed_args.image:
        parser.error('Message is missing')
    return parsed_args


def main():
    "run script"
    # read the args
    args = arguments()
    if args.action == 'save' or args.action == 'max_chars' or args.action == 'read':
        # image name and copy
        name, ext = os.path.splitext(args.image)
        copy_name = '%s_copy%s' % (name, ext)
        # open the image
        try:
            image = Image.open(args.image)
        except IOError as error:
            sys.exit(error)
    # check if the message it's a string or file
    if args.message[0] == '@':
        message = ''
        # open the file
        filename = args.message[1:]
        message_raw = open(filename, 'r')
        for line in message_raw:
            message += line
        message = message.encode('utf-8')
    else:
        message = args.message.encode('utf-8')
    # act according
    if args.action == 'min_image':
        response = 'total message length is %s, min images size is %s'
        print response % min_image(message, args.offset, args.sep)
    if args.action == 'max_chars':
        response = 'Image %s allows %d chars'
        print response % (args.image, max_chars(image))
    if args.action == 'save':
        response = save_message(
            message, image, copy_name, args.offset, args.sep)
        print response
    if args.action == 'read':
        response = read_message(image)
        print response

if __name__ == '__main__':
    main()
