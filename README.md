Python
======

my python scripts

I'm a huge fan of python and I'm always working with script languages.
This is my repository of what I want to remember using python and some experiments.

### Pip
* http://pip.readthedocs.org/en/latest/installing.html
* wget https://bootstrap.pypa.io/get-pip.py
* python get-pip.py

### Pylint
* pip install pylint
* pylint file.py

### PEP8
* [https://pep8.readthedocs.org/en/latest/intro.html](https://pep8.readthedocs.org/en/latest/intro.html)
* apt-get install pep8
* pep8 file.py

### AutoPEP
* apt-get install python-autopep8
* autopep8 file.py
* autopep8 -i file.py