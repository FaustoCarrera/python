#!/usr/bin/env python
#-*- coding: utf-8 -*-
# pymysql example

import ConfigParser
import pymysql


def setup():
    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'mysql.conf'
    configParser.read(configFilePath)
    host = configParser.get("config", "host")
    user = configParser.get("config", "user")
    password = configParser.get("config", "password")
    db = configParser.get("config", "db")
    return {"host": host, "user": user, "password": password, "db": db}


def connect(data):
    connection = pymysql.connect(
        host=data["host"],
        user=data["user"],
        passwd=data["password"],
        db=data["db"],
        unix_socket="/var/run/mysql/mysql.sock"
        )
    return connection


def readLogs(conn):
    q = "SELECT * FROM logs"
    cur = conn.cursor()
    cur.execute(q)
    result = cur
    cur.close()
    conn.close()
    return result


if __name__ == "__main__":
    config = setup()
    conn = connect(config)
    logs = readLogs(conn)
    for l in logs:
        print "%s, %s" % (l[0], l[1])
