#!/usr/bin/env python
#-*- coding: utf-8 -*-
# mysql connector example

import ConfigParser
import mysql.connector
from mysql.connector import errorcode


def setup():
    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'mysql.conf'
    configParser.read(configFilePath)
    host = configParser.get("config", "host")
    user = configParser.get("config", "user")
    password = configParser.get("config", "password")
    db = configParser.get("config", "db")
    return {"host": host, "user": user, "password": password, "database": db}


def connect(config):
    try:
        connection = mysql.connector.connect(**config)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print "Error wrong username or password"
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print "Database does not exists"
        else:
            print err
    return connection


def readLogs(conn):
    result = []
    q = "SELECT * FROM logs"
    cur = conn.cursor()
    cur.execute(q)
    for (id, errorno, errtype, errstr, errfile, errline, time) in cur:
        result.append({
            "id": id,
            "errorno": errorno,
            "errtype": errtype,
            "errstr": errstr,
            "errfile": errfile,
            "errline": errline,
            "time": time
            })
    cur.close()
    conn.close()
    return result


if __name__ == "__main__":
    config = setup()
    conn = connect(config)
    logs = readLogs(conn)
    for l in logs:
        print "{}, {}, {:%d %b %Y}".format(l["id"], l["errstr"], l["time"])
