#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import mmap


def numlines(filename):
    try:
        f = open(filename, "r+")
        buf = mmap.mmap(f.fileno(), 0)
        lines = 0
        readline = buf.readline
        while readline():
            lines += 1
    except:
        lines = 0
    return int(lines)


def main(filename):
    print numlines(filename)

if __name__ == "__main__":
    args = sys.argv[1:]
    main(args[0])
