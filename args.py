#/us/bin/env python
# -*- coding: utf-8 -*-

import sys


def main(args):
    print args


def args():
    args = {}
    for arg in sys.argv[1:]:
        if "=" in arg:
            parts = arg.split("=")
            args[parts[0]] = parts[1]
return args


if __name__ == "__main__":
    args = args()
    main(args)
