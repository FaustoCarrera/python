#!/usr/bin/env python
#-*- coding: utf-8 -*-

# http://simeonfranklin.com/blog/2012/jul/1/python-decorators-in-12-steps/
# Decorators are hard to understand!
# etting decorators requires understanding several functional programming concepts as well
# as feeling comfortable with some unique features of Python’s function definition and function
# calling syntax. *Using* decorators is easy! But writing them can be complicated.


global_str = "global variable"

# In Python functions create a new scope.
# Pythonistas might also say that functions have their own namespace.
# This means Python looks first in the namespace of the function to find variable names when it
# encounters them in the function body.
# LEGB: Local, Enclosing, Global, Bulit-in
# Python does allow us to pass arguments to functions.
# The parameter names become local variables in our function.

def foo(x):
    local_str = 'local variable'
    print locals()


def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def apply(func, x, y):
    return func(x, y)

# Python allows the creation of nested functions.
# This means we can declare functions inside of functions and all the scoping and lifetime rules
# still apply normally.

def outer(x):
    def inner():
        print "this is inner with %u" % x
    return inner


def outer_f(some_func):
    def inner():
        print "before some_func"
        ret = some_func()
        return ret * 2
    return inner


def bin():
    return 1


def ban():
    return "blah "


class Coordinates(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return str(self.__dict__)


def wrapper(func):
    def checker(a, b):
        if a.x < 0 or a.y < 0:
            a = Coordinates(
                a.x if a.x > 0 else 0,
                a.y if a.y > 0 else 0
                )
        if b.x < 0 or b.y < 0:
            b = Coordinates(
                b.x if b.x > 0 else 0,
                b.y if b.y > 0 else 0)
        ret = func(a, b)
        if ret.x < 0 or ret.y < 0:
            ret = Coordinates(
                ret.x if ret.x > 0 else 0,
                ret.y if ret.y > 0 else 0
                )
        return ret
    return checker


@wrapper
def add_coords(a, b):
    print "add coords"
    return Coordinates(a.x + b.x, a.y + b.y)


@wrapper
def sub_coords(a, b):
    print "sub coords"
    return Coordinates(a.x - b.x, a.y - b.y)


def logger(func):
    def inner(*args, **kwargs):
        print "arguments: %s, %s" % (args, kwargs)
        return func(*args, **kwargs)
    return inner


@logger
def lorem(x, y=1):
    return x + y


@logger
def ipsum():
    return 5

if __name__ == "__main__":
    print '=== globals ==='
    print globals()

    print '=== locals ==='
    foo('another local variable')
    print issubclass(foo.__class__, object)

    print apply(add, 5, 4)
    print apply(sub, 5, 4)
    bar = outer(1)
    baz = outer(2)
    bar()
    baz()
    bin = outer_f(bin)
    print bin()  # decorated version of bin
    ban = outer_f(ban)
    print ban()  # decorated version ogf ban
    # Coordinates
    one = Coordinates(100, 200)
    two = Coordinates(300, 400)
    print add_coords(one, two)
    print sub_coords(one, two)
    # logger
    lorem(666)
    ipsum()
