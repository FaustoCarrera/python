#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


def main(filename, sort):
    words_list = {}
    total = 0
    try:
        f = open(filename, "r")
    except:
        print "error trying ot open file %s" % filename

    for line in f:
        words = line.split(' ')
        for word in words:
            word = word.replace(",", "").replace(".", "").lower().strip()
            if word != "":
                total = total + 1
                if word in words_list:
                    words_list[word] = words_list[word] + 1
                else:
                    words_list[word] = 1
    words_result(words_list, sort, total)


def words_result(words, sort, total):
    if sort == "wd":
        keys = sorted(words.keys())
        for key in keys:
            format_output(key, words[key])
    elif sort == "qt":
        qt = {}
        for key in words.keys():
            k = "%s_%s" % (words[key], key)
            qt[k] = key

        keys = sorted(qt.keys(), reverse=True)
        for key in keys:
            format_output(key, qt[key])

    print "Total: %d" % total


def format_output(key, value):
    p = key.split("_")
    print "%s %s" % (p[0], value)


if __name__ == "__main__":
    #remove the script name
    args = sys.argv[1:]
    #check the arguments
    if not len(args):
        print "Usage: python %s text_file sort" % sys.argv[0]
        print "text_file is the name of the file to read"
        print "sort is the type of sort: wd for word and qt for quantity"
        sys.exit(1)
    # check the sort
    sort_list = ["wd", "qt"]
    sort = sort_list[0]
    if len(args) > 1:
        if args[1] in sort_list:
            sort = args[1]

    main(args[0], sort)
