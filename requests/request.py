#!/usr/bin/env python
#-*- coding: utf-8 -*-
# this is a rest test for
# http://requestb.in
# http://interfere.io

import requests
import time

if __name__ == "__main__":
    url = "http://interfere.io/944/test "
    # url = "http://requestb.in/n6gm3cn6"
    r = requests.post(url, data={"timestamp":time.time()})
    print r.status_code
    print r.content