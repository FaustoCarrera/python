#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script makes a backup and restore all the documents on a bucket
dependencies:
pip install requests

author: Fausto Carrera <fausto.carrera@gmx.com>
release: 2014-08-01
last update: 2014-09-05
"""

import os
import sys
import json
import base64
import math
import time
from lib.couchbase import Couchbase
from lib.tools import Tools


class Backup(object):

    "Backup class"
    bucket = None
    couchbase = None
    dest_folder = None
    tools = None

    def __init__(self, config_file, couchbase_bucket):
        self.bucket = bucket
        self.couchbase = Couchbase(
            os.path.abspath(config_file), couchbase_bucket)
        self.dest_folder = self.setup_destination(bucket)
        self.tools = Tools()

    def backup(self, view_name):
        "backup documents"
        # the view
        if view_name != '_all_docs':
            view_parts = view_name.split('/')
            view_name = "_design/%s/_view/%s" % (view_parts[0], view_parts[1])
        # pager
        page = 0
        total_pages = 2
        limit = 1500.0
        while page < total_pages:
            # pager
            skip = limit * page
            view_limit = "%s?limit=%u&skip=%u" % (view_name, limit, skip)
            print "using view %s" % view_limit
            result = self.couchbase.view(view_limit)
            total_rows = result['total_rows']
            total_pages = math.ceil(total_rows / limit)
            for row in result['rows']:
                doc_id = base64.b64encode(b"%s" % row['id'])
                dest = "%s/%s" % (self.dest_folder, doc_id)
                doc = self.couchbase.read(row['id'])
                status = self.tools.send_to_file(json.dumps(doc), dest)
                print "doc %s saved as %s: %s" % (row['id'], doc_id, status)
            page += 1
            print "page %s of %s" % (page, int(total_pages))
            print '-' * 20
            time.sleep(2)
        print "total rows: %s" % total_rows

    def restore(self, couchbase_bucket, source_path):
        "restore files to bucket"
        print "%s %s" % (couchbase_bucket, source_path)

    def setup_destination(self, couchbase_bucket):
        "setup destination folder"
        destination_folder = "./docs/%s/" % couchbase_bucket
        destination_folder = os.path.abspath(destination_folder)
        if not os.path.exists(destination_folder):
            try:
                os.makedirs(destination_folder)
                return destination_folder
            except Exception:
                return None
        return destination_folder

if __name__ == '__main__':
    tools = Tools()
    args = tools.arguments(sys.argv)
    # args
    action = tools.get_defaults(args, 'action', None)
    bucket = tools.get_defaults(args, 'bucket', None)
    view = tools.get_defaults(args, 'view', '_all_docs')
    source = tools.get_defaults(args, 'source', None)
    # actions
    actions = ['backup', 'restore']
    if action and action in actions:
        # backup
        bkp = Backup('config/juanvm.conf', bucket)
        if action == 'backup':
            bkp.backup(view)
        if action == 'restore':
            bkp.restore(bucket, source)
    else:
        print "usage: %s --action=[backup|restore] --bucket=billing" % sys.argv[0]
        print "in case of backup include the view to read the docs: --view=[_all_docs|clients/by_fb_id]"
        print "in case of restore include the source of the docs: --source=./docs/clients/"
