#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
Tools
"""

import sys


class Tools(object):

    "Tools class"

    def __init__(self):
        pass

    # parse arguments

    def arguments(self, raw_args):
        "Parse arguments"
        args = {}
        raw_args = raw_args[1:]
        for arg in raw_args:
            if '--' in arg:
                parts = arg.split('=')
                key = parts[0].replace('--', '')
                args[key] = parts[1]
            elif '-' in arg:
                key = arg.replace('-', '')
                args[key] = True
        return args

    # get default value

    def get_defaults(self, args, key, default):
        "try to return the value or return the default"
        try:
            value = args[key]
        except Exception:
            value = default
        return value

    # write data to file

    def send_to_file(self, data, dest):
        "save data to file"
        try:
            fopen = open(dest, 'w')
        except Exception, error:
            print "Error writing to file %s: %s" % (dest, error)
            return False
        fopen.write(data)
        fopen.close()
        return True

if __name__ == '__main__':
    tools = Tools()
    arguments = tools.arguments(sys.argv)
    print arguments
    lorem = tools.get_defaults(arguments, 'lorem', '-*-ipsum-*-')
    print lorem
