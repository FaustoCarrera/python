#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This class interacts with couchbase
"""

import ConfigParser
import json
import requests


class Couchbase(object):
    "Couchbase class"

    couchbase = {}

    def __init__(self, config_file, bucket):
        self.setup(config_file, bucket)

    # crud

    def create(self, did, data):
        "create a new document on couchbase bucket"
        couchbase_url = self.url(did)
        headers = {'Content-type': 'text/plain'}
        if type(data) is dict:
            headers = {'Content-type': 'application/json'}
            data = json.dumps(data)
        elif type(data) is int:
            data = str(data)

        req = requests.put(
            couchbase_url,
            auth=self.couchbase['auth'],
            headers=headers,
            data=data,
        )
        return req.json()

    def read(self, did):
        "read a document on couchbase bucket"
        couchbase_url = self.url(did)
        headers = {'Content-type': 'application/json'}
        req = requests.get(
            couchbase_url,
            auth=self.couchbase['auth'],
            headers=headers,
        )
        try:
            return req.json()
        except Exception:
            return req.text

    def update(self, did, data):
        "update a document on couchbase bucket"
        return self.create(did, data)

    def delete(self, did):
        "delete a document from couchbase bucket"
        couchbase_url = self.url(did)
        headers = {'Content-type': 'application/json'}
        req = requests.delete(
            couchbase_url,
            auth=self.couchbase['auth'],
            headers=headers,
        )
        return req.json()

    # views

    def view(self, view):
        "read documents from a view"
        # connect
        couchbase_url = self.url(view)
        # request
        headers = {'Content-type': 'application/json'}
        req = requests.get(
            couchbase_url,
            auth=self.couchbase['auth'],
            headers=headers,
        )
        result = req.json()
        return result

    def all_docs(self):
        "read all documents on the couchbase bucket"
        return self.view('_all_docs')

    # setup

    def url(self, obj=''):
        "setup couchbase url"
        return "http://{0}:8091/couchBase/{1}/{2}".format(
            self.couchbase['host'],
            self.couchbase['bucket'],
            obj)

    def setup(self, filename, bucket):
        "setup configuration file"
        config_parser = ConfigParser.RawConfigParser()
        config_file_path = r"%s" % filename
        config_parser.read(config_file_path)
        host = config_parser.get('config', 'host')
        user = config_parser.get('config', 'user')
        password = config_parser.get('config', 'password')
        self.couchbase = {
            'host': host,
            'auth': (user, password),
            'bucket': bucket
        }

if __name__ == '__main__':
    couch = Couchbase('./config/couchbase.conf', 'test')

    # create a new document
    doc_id = 'test::test'
    doc = {"lorem": "ipsum"}
    print couch.create(doc_id, doc)

    # read document
    print couch.read(doc_id)

    # update document
    doc['dolor'] = 'sit amet'
    print couch.update(doc_id, doc)

    # delete document
    print couch.delete(doc_id)

    # get documents from view
    docs = couch.view('_all_docs') # couch.all_docs()
    print "total docs: %u" % int(docs['total_rows'])
    for doc in docs['rows']:
        print doc
