#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
EXIF images data
https://github.com/ianare/exif-py
"""

import os
import click
import exifread
import hashlib

__version__ = '0.1.0'
__updated__ = '2018-02-22'


def get_tags(tags):
    "Display images tags"
    for tag in tags.keys():
        if tag is not 'JPEGThumbnail':
            print('{0}: {1}'.format(tag, tags[tag]))


def export_thumb(tags, filename):
    "Export image thumb"
    if tags['JPEGThumbnail']:
        hasher = hashlib.md5()
        hasher.update(filename)
        filename = './{0}_thumb.jpg'.format(hasher.hexdigest())
        file = open(filename, 'w')
        file.write(tags['JPEGThumbnail'])
        file.close()
    else:
        print('No thumbnail')


def geo_link(tags):
    "Get image geolocation"
    url = 'https://www.google.com.ar/maps/@%f,%f,16z?hl=en'
    print(url % get_gps_coords(tags))


def get_gps_coords(tags):
    "Get GPS coords"
    # longitude
    lon_ref_tag_name = 'GPS GPSLongitudeRef'
    lon_tag_name = 'GPS GPSLongitude'
    # latitude
    lat_ref_tag_name = 'GPS GPSLatitudeRef'
    lat_tag_name = 'GPS GPSLatitude'
    # Check if these tags are present
    gps_tags = [lon_ref_tag_name, lon_tag_name, lat_tag_name, lat_tag_name]
    for tag in gps_tags:
        if not tag in tags.keys():
            return None
    # get the latitude
    lat_ref_val = tags[lat_ref_tag_name].values
    lat_dir = lat_ref_val[0]
    lat_ref_coord = tags[lat_tag_name].values
    lat_coords_vals = [float(lat_ref_coord[0].__repr__()), float(lat_ref_coord[1].__repr__())]
    lat_coords = sum([c / 60 ** i for i, c in enumerate(lat_coords_vals)])
    lat_coords *= (-1) ** (lat_dir == 'S')
    # get the longitude
    lon_ref_val = tags[lon_ref_tag_name].values
    lon_dir = lon_ref_val[0]
    lon_ref_coord = tags[lon_tag_name].values
    lon_coords_vals = [float(lon_ref_coord[0].__repr__()), float(lon_ref_coord[1].__repr__())]
    lon_coords = sum([c / 60 ** i for i, c in enumerate(lon_coords_vals)])
    lon_coords *= (-1) ** (lon_dir == 'W')
    # return data
    return (lat_coords, lon_coords)


@click.command()
@click.argument('imagepath', type=click.Path(exists=True))
@click.option('--data', '-d', 'action', flag_value='tags', default=True, help='Show image data')
@click.option('--geo', '-g', 'action', flag_value='geo', default=False, help='Show image geolocation')
@click.option('--thumb', '-t', 'action', flag_value='thumb', default=False, help='Export image thumbnail')
def run(imagepath, action):
    "Run image exif data extractor"
    file = open(os.path.realpath(imagepath), 'rb')
    tags = exifread.process_file(file)
    if action == 'tags':
        get_tags(tags)
    if action == 'thumb':
        export_thumb(tags, imagepath)
    if action == 'geo':
        geo_link(tags)


if __name__ == '__main__':
    run()
