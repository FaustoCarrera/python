#!/usr/bin/env python
# -*- coding: utf-8 -*-

import ConfigParser

configParser = ConfigParser.RawConfigParser()
configFilePath = r'default.conf'
configParser.read(configFilePath)
print configParser.get("config", "lorem")
print configParser.get("config", "dolor")
