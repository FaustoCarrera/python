#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import cypher
import getopt
from getpass import getpass

def help():
    usage = "Usage: %s -[e(encrypt) | d(decrypt) | p(print in screen)] file_in [file_out]" % sys.argv[0]
    print usage;
    sys.exit()

def writefile(outfile_name, file_buffer):
    outfile = open(outfile_name, 'wb')
    outfile.write(file_buffer)
    outfile.close()

def filename(file_name):
    parts = file_name.split('.')
    return "%s.bf" % parts[0]

if __name__ == "__main__":
    try: opts, args = getopt.getopt(sys.argv[1:], 'e:d:p:')
    except getopt.GetoptError: help()

    opts = dict(opts)
    try: mode = opts.keys()[0]
    except IndexError: help()

    file_in = opts[mode]
    try: file_out = args[0]
    except IndexError: file_out = filename(file_in)

    try:
        infile = open(file_in, 'rb')
        filebuffer = infile.read()
        infile.close()
    except:
        print "file '%s' does not exist.\n" % file_in
        sys.exit()

    key = getpass()

    bfc = cypher.BFCipher(key)
    if mode == '-e':
        filebuffer = bfc.encrypt(filebuffer)
        writefile(file_out, filebuffer)
    elif mode == '-d':
        filebuffer = bfc.decrypt(filebuffer)
        writefile(file_out, filebuffer)
    elif mode == '-p':
        sys.stdout.write(bfc.decrypt(filebuffer))

    key = 'x'*len(key);
    del key