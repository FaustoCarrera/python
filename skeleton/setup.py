#!/usr/bin/env python
# -*- coding: utf-8 -*-
# python packages needed to install:
# pip: http://pypi.python.org/pypi/pip
# distribute: : http://pypi.python.org/pypi/distribute
# nose: : http://pypi.python.org/pypi/nose
# virtualenv: : http://pypi.python.org/pypi/virtualenv

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    "name": "project name",
    "description": "My project",
    "author": "Fausto Carrera",
    "author_email": "fausto.carrera.f@gmail.com",
    "url": "http://...",
    "download_url": "http://github.com/...",
    "version": "0.1",
    "install_requires": ["nose"],
    "packages": ["PROJECT", "bin"],
    "scripts": []
}

setup(**config)
