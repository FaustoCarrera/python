#!/usr/bin/env python
# -*- codign: utf-8 -*-
# setup:
# The software ffmpeg is needed for writing, reading, converting the sound and the video.
# imageMagick is needed for all text generation, GIF support, and much more in the future.
# (sudo) pip install decorator
# (sudo) pip install numpy
# (sudo) pip install moviepy
# (sudo) apt-get install python-numpy python-scipy
# sudo apt-get install gfortran libopenblas-dev liblapack-dev to install Scipy
# Scipy is needed for many advanced functionalities (tracking, segmenting, etc.), and can be used for resizing video clips if PIL and OpenCV aren't installed on your computer.
# PyGame is needed for video and sound previews (really essential for advanced editing).
# Scikit Image may be needed for some advanced image manipulation.
# OpenCV 2.4.6 (which provides the python package cv2) or more recent may be needed for some advanced image manipulation. See below for the installation of OpenCV.

from moviepy.editor import *
VideoFileClip("the_crack.mp4").subclip((1,22.65),(1,23.2)).to_gif("use_your_head.gif")
