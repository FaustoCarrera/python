#define a function
def sumseq(seq, num = 0):
    res = []
    for x in seq:
        res.append(x + num)
    return res

L = [1, 2, 3, 4]
M = sumseq(L[:], 1) #send a copy
print(L, M)

#lambda functions
x = (lambda x, y : x + y)
print(x(1, 2))
print(x('lorem', 'ipsum'))