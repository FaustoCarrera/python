#/usr/bin/env python
#-*- coding: utf-8 -*-


class NGram(object):

    def __init__(self, text, n=3):
        self.n = n
        self.length = 0
        self.table = {}
        self.parse_text(text)

    def parse_text(self, text):
        chars = " " * self.n  # initial sequence
        for letter in (" ".join(text.split()) + " "):
            chars = chars[1:] + letter  # append letter to the sequence
            self.table[chars] = self.table.get(chars, 0) + 1
        self.calculate_length()

    def calculate_length(self):
        """ Treat the N-Gram table as a vector and return its scalar magnitude
        to be used for performing a vector-based search.
        """
        self.length = sum([x * x for x in self.table.values()]) ** 0.5
        return self.length

    def __sub__(self, other):
        """ Find the difference between two NGram objects by finding the cosine
        of the angle between the two vector representations of the table of
        N-Grams. Return a float value between 0 and 1 where 0 indicates that
        the two NGrams are exactly the same.
        """
        if not isinstance(other, NGram):
            raise TypeError("Can't compare NGram with no-NGram object")
        if self.n != other.n:
            raise TypeError("Can't compare objects of different size")
        total = 0
        for k in self.table:
            total += self.table[k] * other.table.get(k, 0)
        return 1.0 - (float(total) / (float(self.length) * float(other.length)))

    def best_match(self, languages):
        """ Out of a list of NGrams that represent individual languages, return
        the best match.
        """
        result = {}
        for lang in languages:
            result[self - languages[lang]] = lang
        return result[min(result.keys())]
        #return min(languages, key=lambda n: self - n)

if __name__ == "__main__":
    english = NGram("Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")
    spanish = NGram("Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la creación de las hojas Letraset, las cuales contenian pasajes de Lorem Ipsum, y más recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.")
    latin = NGram("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vitae malesuada arcu. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus nibh justo, rutrum in vulputate at, venenatis nec dolor. Nam gravida placerat dapibus. Ut pharetra leo a orci faucibus fringilla. Integer posuere placerat nunc eget gravida. Fusce ultrices metus dui, sit amet semper nulla gravida nec. Proin turpis sapien, semper varius erat eu, tempor imperdiet tortor. Cras at sem ac mi pretium facilisis. Morbi vel blandit sapien, vitae luctus mi.")
    # test
    english_test = NGram("printing and typesetting industry")
    spanish_test = NGram("hola que tal, como va")
    latin_test = NGram("Aenean vitae malesuada arcu")
    # similarity
    print "English: %s" % (english - english_test)
    print "Spanish: %s" % (spanish - spanish_test)
    print "Latin: %s" % (latin - latin_test)
    print "English vs Latin: %s" % (english - latin)
    print "Spanish vs Latin: %s" % (spanish - latin)
    print "English vs Spanish: %s" % (english - spanish)
    # vector-based search
    languages = {"english": english, "spanish": spanish, "latin": latin}
    # print english_test.best_match(languages)
    print spanish_test.best_match(languages)
    # print latin_test.best_match(languages)
