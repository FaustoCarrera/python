#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pika
import sys

def main():
    print "============================================="
    print "Usage: %s [action] queue=... message=..." % sys.argv[0]
    print "Actions:"
    print "help: print this message"
    print "publish: publish message to queue"
    print "============================================="

def publish(queue, body):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    channel.basic_publish(exchange='', routing_key=queue, body=body)
    print " [x] Message sent"
    connection.close()

def consume(queue):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue=queue)
    print ' [*] Waiting for messages. To exit press CTRL+C'
    channel.basic_consume(callback, queue=queue)
    channel.start_consuming()

def callback(ch, method, properties, body):
    print " [x] Received %r" % (body,)
    ch.basic_ack(delivery_tag = method.delivery_tag)

# get the suplied args
def args():
    args = {}
    for arg in sys.argv[1:]:
        if "=" in arg:
            parts = arg.split("=")
            args[parts[0]] = parts[1]
    return args

# get argument or set default
def get_arg(pos, default):
    try:
        val = sys.argv[pos]
    except:
        val = default
    return val

if __name__ == "__main__":
    action = get_arg(1, "help")
    args = args()

    if action == "help":
        main()

    if action == "publish":
        publish(args["queue"], args["message"])

    if action == "consume":
        consume(args["queue"])