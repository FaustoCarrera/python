#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import requests
import ConfigParser

cloudant = {}


def set_doc(key, data):
    # connect
    post_url = url(cloudant["auth"], cloudant["db"])
    # format data
    if key:
        data["_id"] = key
    # request
    headers = {'Content-type': 'application/json'}
    r = requests.post(
        post_url,
        auth=cloudant["auth"],
        headers=headers,
        data=json.dumps(data)
    )
    doc = r.json()
    return doc["id"]


def get_doc(key):
    post_url = url(cloudant["auth"], cloudant["db"]) + key
    r = requests.get(post_url, auth=cloudant["auth"])
    return r.json()


def update_doc(key, data):
    # get the doc
    doc = get_doc(key)
    post_url = url(cloudant["auth"], cloudant["db"])
    # compose
    for k in data.keys():
        doc[k] = data[k]
    # request
    headers = {'Content-type': 'application/json'}
    r = requests.post(
        post_url,
        auth=cloudant["auth"],
        headers=headers,
        data=json.dumps(data)
    )
    return r.json()


def delete_doc(key):
    doc = get_doc(key)
    post_url = url(cloudant["auth"], cloudant["db"]) + key
    payload = {"rev": doc["_rev"]}
    r = requests.delete(post_url, auth=cloudant["auth"], params=payload)
    return r.json()


def delete_bulk(data):
    post_url = url(cloudant["auth"], cloudant["db"]) + "_bulk_docs"
    # request
    headers = {'Content-type': 'application/json'}
    r = requests.post(
        post_url,
        auth=cloudant["auth"],
        headers=headers,
        data=json.dumps(data)
    )
    return r.json()


def get_all_docs():
    post_url = url(cloudant["auth"], cloudant["db"]) + "_all_docs"
    r = requests.get(post_url, auth=cloudant["auth"])
    return r.json()


def url(auth, db):
    post_url = "http://{0}.cloudant.com/{1}/".format(auth[0], db)
    return post_url

# tests


def test_delete_bulk():
    docs = get_all_docs()
    bulkdocs = {"docs": []}
    print "Total: %u" % docs["total_rows"]
    print "Offset: %u" % docs["offset"]
    for row in docs["rows"]:
        doc = {
            "_id": row["id"],
            "_rev": row["value"]["rev"],
            "_deleted": True
        }
        bulkdocs["docs"].append(doc)
    print delete_bulk(bulkdocs)


def setup():
    configParser = ConfigParser.RawConfigParser()
    configFilePath = r'cloudant.conf'
    configParser.read(configFilePath)
    user = configParser.get("config", "user")
    password = configParser.get("config", "password")
    db = configParser.get("config", "db")
    return {"auth": (user, password), "db": db}

if __name__ == "__main__":
    cloudant = setup()

    data = {
        "type": "test_doc",
        "lorem": "ipsum",
        "dolor": "sit amet",
        "consectetur": "adipiscing elit",
        "aliquam": "porta neque id",
        "ante": "aliquet ornare",
        "ver": 1
    }
    # new document
    doc_id = set_doc(None, data)
    print doc_id
    # read document
    # print get_doc(doc_id)
    # update document
    # update_data = {"ver": 666}
    # print update_doc(doc_id, update_data)
    # delete document
    # print delete_doc(doc_id)
