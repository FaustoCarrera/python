#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys


def main(filename, start, end):
    count = 1
    if start > end:
        end = start + 10

    try:
        f = open(filename, "r")
    except:
        sys.exit("Error trying to open file %s" % filename)

    for line in f:
        if count >= start and count <= end:
            print line,
        count = count + 1
        if count > end:
            break


def help():
    print "Usage: %s filename start end" % sys.argv[0]
    print "catlines works like head or tail, but it could start on any line"
    print "and end on any line"


def get_arg(pos, default):
    try:
        val = sys.argv[pos]
    except:
        val = default
    return val

if __name__ == "__main__":
    args = sys.argv[1:]

    if args[0] == "--help":
        help()
    else:
        filename = get_arg(1, "-")
        start = get_arg(2, 1)
        end = get_arg(3, 10)
        main(filename, int(start), int(end))
