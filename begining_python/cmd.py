#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import getopt

params = sys.argv[1:]

#opts, args = getopt.getopt(params, 'hc:', ['help', 'config='])
opts, args = getopt.gnu_getopt(params, 'hc:', ['help', 'config='])
print opts, args

for option, parameter in opts:
    print "%s => %s" % (option, parameter)
