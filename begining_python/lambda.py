#!/usr/bin/env python
#-*- coding: utf-8 -*-

numbers_list = range(25)
numbers_tuple = [(i, j) for i, j in enumerate(numbers_list)]

print numbers_list
print numbers_tuple

# Filter enables you to take a list and remove elements based on criteria you
# define within a function you write.

print "-=-=-=-= filter =-=-=-=-"

print "lambda x: x % 2 == 0"
result = filter(lambda x: x % 2 == 0, numbers_list)
print result

# Reduce is a way to take the elements of a list or a tuple and run a function
# on the first two elements, then it uses the result of that operation and runs
# the result and the next element in the list through the same operation.

print "-=-=-=-= reduce =-=-=-=-"

print "lambda x, y: x + y"
result = reduce(lambda x, y: x + y, numbers_list)
print result

# Map is a special function for cases when you need to do a specific action on
# every element of a list. It enables you to accomplish this without having to
# write the loop.

print "-=-=-=-= map =-=-=-=-"

print "lambda x: x ** 2"
result = map(lambda x: x ** 2, numbers_list)
print result

print "lambda x: x % 2"
result = map(lambda x: x % 2, numbers_list)
print result

print "lambda x: x[0] * x[1]"
result = map(lambda x: x[0] * x[1], numbers_tuple)
print result

# List comprehension feature entered the language in Python 2.0. It enables you
# to write miniature loops and decisions within the list dereferencing
# operators (the square brackets) to define parameters that will be used to
# restrict the range of elements being accessed.

print "-=-=-=-= list comprehension =-=-=-=-"

print "x ** 2 for x in numbers_list"
result = [x ** 2 for x in numbers_list]
print result

print "x for x in numbers_list if x % 2 == 0"
result = [x for x in numbers_list if x % 2 == 0]
print result

# xrange creates fewer elements in memory than range, so it is perfect for
# really large sets of numbers.
# Xrange produces an object that doesn’t have any public methods. The only
# methods it has are built-in methods that enable it to act as a very simple
# sequence.

print "-=-=-=-= xrange =-=-=-=-"

print "lambda x, y: x + y"
xr = xrange(0, 500000)
result = reduce(lambda x, y: x + y, xr)
print result
