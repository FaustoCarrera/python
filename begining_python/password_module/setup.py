#!/usr/bin/env python
#-*- coding: utf-8 -*-
# run as python setup.py sdist to generate dist package

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    "name": "passwords",
    "description": "Secure passwords generator in python",
    "author": "Fausto Carrera",
    "author_email": "fausto.carrera@gmx.com",
    "url": "http://faustocarrera.com.ar",
    "download_url": "http://github.com/faustocarrera/python",
    "version": "0.1",
    "py_modules": ["passwords"],
}

setup(**config)
