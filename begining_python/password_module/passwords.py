#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
Module for making safe password in Python
"""

import hashlib
import random
import base64


class password(object):

    """
    Generates random password
    creates hashes for passwords
    checks a hash against a password
    """

    def __init__(self):
        pass

    def _get_salt(self):
        """Creates a salt string"""
        salt = [chr(random.randint(0, 255)) for i in range(8)]
        return ''.join(salt)

    def make_hash(self, password):
        """make a hash from a string"""
        salt = self._get_salt()
        text = salt + password
        hash = hashlib.sha512(text).digest()
        data = salt + hash
        return base64.encodestring(data)

    def check_hash(self, cipher, password):
        """Check if the hash and the hash from the password string are the
        same"""
        cipher = base64.decodestring(cipher)
        salt, hash = cipher[:8], cipher[8:]
        hash2 = hashlib.sha512(salt + password).digest()
        return hash2 == hash

    def generate(self, pass_len=12, special_chars=False):
        """Generate a new random password"""
        password = ""
        letters = "abcdefghijklmnopqrstuvwxyz"
        letters += "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        letters += "0123456789"
        if special_chars:
            letters += "!#$%&(){}[]"
        letters_len = len(letters)
        while len(password) < pass_len:
            try:
                l = random.randint(0, letters_len)
                password = password + letters[l]
            except:
                pass
        return password


if __name__ == "__main__":
    passwd = password()
    # generate a new password
    generated = passwd.generate(12)
    print generated
    # generate a hash fro mthe password
    pwd_hash = passwd.make_hash(generated)
    print pwd_hash
    # check if the hash is correct
    print passwd.check_hash(pwd_hash, generated)
