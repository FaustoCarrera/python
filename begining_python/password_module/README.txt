## Passwords ##

This class:
Generates random passwords
Creates hashes for passwords
Checks a hash against a password

check_hash(cipher, password)
    Check if the hash and the hash from the password string are the same

generate(pass_len=12, special_chars=False)
    Generate a new random password
    The default len is 12 chars
    if special chars is true, this will include chars like ([#!%])

make_hash(password)
	make a hash from a string

## Use ##
from passwords import password

pwd = password()
rand_pass = pwd.generate(12, True)
pass_hash = pwd.make_hash(rand_pass)
is_valid = pwd.check_hash(pass_hash, rand_pass)