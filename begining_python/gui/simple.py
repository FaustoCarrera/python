#!/usr/bin/env python
#-*- coding: utf-8 -*-

import pygtk
pygtk.require('2.0')
import gtk


class Single_button_GUI(object):

    CLICK_COUNT = "Click count: %d"

    def __init__(self):
        """Setup window and the button"""
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_border_width(10)
        # the button
        self.button = gtk.Button(self.CLICK_COUNT % 0)
        self.button.connect("clicked", self.clickButton)
        self.button.times_clicked = 0
        self.window.add(self.button)
        # destroy the widget if window is closed
        self.window.connect("destroy", self.destroy)
        # show the GUI
        self.button.show()
        self.window.show()

    def clickButton(self, widget):
        self.button.times_clicked += 1
        self.button.set_label(self.CLICK_COUNT % self.button.times_clicked)

    def destroy(self, window):
        """Remove the window and quit the program"""
        window.hide()
        gtk.main_quit()
        print "bye bye"

    def main(self):
        gtk.main()

if __name__ == "__main__":
    sbgui = Single_button_GUI()
    sbgui.main()
