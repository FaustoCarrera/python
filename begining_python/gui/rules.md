##RULES##

1. Your *GUI.py is the only Python file allowed to call GTK functions
2. Only one thread is allowed to run in *GUI.py.
3. The thread in *GUI.py will read from and clear a GUI queue object; other threads will add actions to the queue object.
4. For any operation that might take a long time to complete, your GUI will start another worker thread. This especially includes network calls.

The term *GUI.py means that once you’ve decided on a name for your program, you’ll create nameGUI.py so that you know it will be the file that follows these rules.

