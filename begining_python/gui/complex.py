#!/usr/bin/env python
#-*- coding: utf-8 -*-

import pygtk
pygtk.require('2.0')
import gtk

class Complex_button_GUI(object):

    def __init__(self):
        # setup window and box
        self.window = gtk.Window()
        self.box = gtk.VBox()
        self.window.add(self.box)
        # add buttons
        self.btn_a = gtk.Button("Lorem")
        self.btn_b = gtk.Button("Ipsum")
        self.box.pack_start(self.btn_a)
        self.box.pack_start(self.btn_b)
        # show gui
        self.btn_a.show()
        self.btn_b.show()
        self.box.show()
        self.window.show()

    def main(self):
        gtk.main()

if __name__ == "__main__":
    cbgui = Complex_button_GUI()
    cbgui.main()