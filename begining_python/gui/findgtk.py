#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
findgtk.py - find the pyGTK libraries, wherever they are
"""

import os
import sys
sys.path.append("/usr/local/lib/python2.7/site-packages")


def try_import():
    """tries to import GTK return boolean"""
    try:
        import pygtk
        pygtk.require("2.0")
    except:
        print "pyGTK not found. You need GTK 2"
        print "Maybe GTK2 is installed but not pyGTK"

    try:
        import gtk
        import gtk.glade
        import gobject
    except:
        import traceback
        traceback.print_exc(file=sys.stdout)
        print "Seems that GTK2 is not installed"
        print "gtk, gtk.glade and gobject import failed"
        return False
    return True

if not try_import():
    site_packages = 0
    if site_packages == 0:
        from stat import *
        check_lib = [
            "/usr/lib/python2.2/site-packages/",
            "/usr/local/lib/python2.2/site-packages/",
            "/usr/local/lib/python2.3/site-packages/"
        ]
        for k in check_lib:
            try:
                path = os.path.join(k, "pygtk.py")
                if open(path) is not None:
                    sys.path = [k] + sys.path
                    if try_import():
                        break
            except:
                pass
    if not try_import():
        sys.exit(0)
