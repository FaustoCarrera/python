#!/usr/bin/env python
#-*- coding: utf-8 -*-

import math
from threading import Thread
import time


class SquareRootCalculator(object):
    """
    This class spawns a separate thread to calculate a bunch
    of square roots, and checks it ones a second till finished
    """

    def __init__(self, target):
        """Turn on calculator and monitor progress"""
        self.results = {}
        counter = calculatorThread(self, target)
        print "Turning on calculator thread"
        counter.start()
        while len(self.results) < target:
            print "%d square roots calculated so far" % len(self.results)
            time.sleep(1)
        for k in self.results:
            print "%s => %s" % (k, self.results[k])


class calculatorThread(Thread):
    """The thread that do the calculations"""

    def __init__(self, controller, target):
        """Setup this thread incluiding making it a daemon"""
        Thread.__init__(self)
        self.controller = controller
        self.target = target
        self.setDaemon(True)

    def run(self):
        """Calculate squeare roots for all numbers in the range"""
        for i in range(1, self.target + 1):
            sqr = math.sqrt(i)
            self.controller.results[i] = sqr

if __name__ == "__main__":
    import sys
    limit = None
    try:
        limit = int(sys.argv[1])
    except ValueError:
        print "Usage: %s [number of square roots to calculate]" % sys.argv[0]
    # calculate
    if limit:
        SquareRootCalculator(limit)
