#!/usr/bin/env python
#-*- coding: utf-8 -*-

import os
import re

wd = os.getcwd()
print "===== current dir ====="
print wd

print "===== paths ====="
print os.path.split(wd)

print "===== dir list ====="
for dir in os.listdir(wd):
    print "file: %s" % dir
    stats = os.stat(dir)
    print stats
    print ""


def find_python(arg, dir, files):
    for f in files:
        if re.search(r"py$", f):
            print os.path.join(dir, f)

os.path.walk(wd, find_python, 0)
