#!/usr/bin/env python
#-*- coding: utf-8 -*-

import string


person = {
    "name": "James",
    "camera": "nikon",
    "handedness": "lefty",
    "baseball_team": "angels",
    "instrument": "guitar"
}
print "%(name)s, %(camera)s, %(baseball_team)s" % person

tpl = '''
$name have a $camera camera, he is $handedness and his baseball team is
$baseball_team, he love to play the $instrument
'''
t = string.Template(tpl)
print t.substitute(person)
