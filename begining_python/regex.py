#!/usr/bin/env python
#-*- coding: utf-8 -*-

import re

s = ("xxx", "abcxxxabc", "xyx", "abc", "x.x", "axa", "axxxxa", "axxya")

# find xxx
print "-~- match xxx -~-"
print filter((lambda s: re.match(r"xxx", s)), s)
# search for xxx
print "-~- search xxx -~-"
print filter((lambda s: re.search(r"xxx", s)), s)
# match an x at start and end and anything in between
print "-~- search x.x -~-"
print filter((lambda s: re.search(r"x.x", s)), s)
# scape special chars
print "-~- search x\.x -~-"
print filter((lambda s: re.search(r"x\.x", s)), s)
# match an x followed by zero or more x
print "-~- search x.*x -~-"
print filter((lambda s: re.search(r"x.*x", s)), s)
# match an x followed by one or more x
print "-~- search x.+x -~-"
print filter((lambda s: re.search(r"x.+x", s)), s)
# match anything with a c
print "-~- search c+ -~-"
print filter((lambda s: re.search(r"c", s)), s)
# match anything without a c
print "-~- search ^[^c]*$ -~-"
print filter((lambda s: re.search(r"^[^c]*$", s)), s)
