#!/usr/bin/env python
#-*- coding: utf-8 -*-

def g(n):
    if n == 0:
        return 0
    if n == 1:
        return 1
    return g(n-1) + g(n-2)

# real    10m3.889s
# user    9m31.198s
# sys     0m32.486s

memo = {0: 0, 1: 1}

def f(n):
    if n not in memo:
        memo[n] = f(n-1) + f(n-2)
    return memo[n]

# real    0m0.056s
# user    0m0.030s
# sys     0m0.023s

def h(x, y, c):
    if c == 0:
        return 0
    if c == 1:
        return 1
    return x + h(y, y+x, c-1)

# real    0m0.046s
# user    0m0.023s
# sys     0m0.019s

def i(n):
    limit = int(n)
    prev = 0
    current = 1
    while limit > 1:
        next = current + prev
        prev = current
        current = next
        limit -= 1
    return current

# real    0m0.047s
# user    0m0.025s
# sys     0m0.020s

def j(n):
    if n <= 0:
        return 0
    i = n - 1
    numbers = {"a": 1, "b": 0, "c": 0, "d": 1}
    a = numbers["a"]
    b = numbers["b"]
    while i > 0:
        if i % 2 == 1:
            a = (numbers["d"] * numbers["b"]) + (numbers["c"] * numbers["a"])
            b = numbers["d"] * (numbers["b"] + numbers["a"]) + (numbers["c"] * numbers["b"])
        else:
            a = numbers["a"]
            b = numbers["b"]
        c = (numbers["c"] ** 2) + (numbers["d"] ** 2)
        d = numbers["d"] * ((numbers["c"] * 2) + numbers["d"])
        # update numbers
        numbers = {"a": a, "b": b, "c": c, "d": d}
        # update i
        i = i / 2
    return a + b

# real    0m0.047s
# user    0m0.025s
# sys     0m0.020s

l = 10000000
# print i(l)
# print f(l)
# print h(0, 1, l)
print j(l)