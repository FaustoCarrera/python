#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This script parses the csv file and returns the mean, the median and the mode
mean: also know as the average, combines all the values in a single value
median: is the middlemost value
mode: is the most common value
"""

import csv
from collections import Counter


class Analysis(object):

    def __init__(self, filename):
        self.filename = filename
        self.result = []

    def parse(self):
        csv_raw = open(self.filename, 'r')
        csv_data = csv.DictReader(csv_raw, delimiter='\t')
        for row in csv_data:
            if row['Notes'] == 'Total':
                break
            # yield (int(row['Year']), int(row['Deaths']))
            yield int(row['Deaths'])

    def mean(self):
        deaths_list = list(self.parse())
        average = sum(deaths_list) / len(deaths_list)
        return average

    def median(self):
        deaths_list = list(self.parse())
        deaths_sorted = sorted(deaths_list)
        if len(deaths_sorted) % 2 == 1:
            return deaths_sorted[len(deaths_sorted) // 2]
        else:
            mid = len(deaths_sorted) // 2
            return (deaths_sorted[mid] + deaths_sorted[mid - 1]) / 2

    def mode(self):
        deaths_list = list(self.parse())
        counter = Counter(deaths_list)
        mode_value, most_common = counter.most_common(1)[0]
        return mode_value

if __name__ == '__main__':
    analysis = Analysis('./cause_of_death_by_year.txt')
    deaths_list = list(analysis.parse())
    print deaths_list
    print analysis.mean()
    print analysis.median()
    print analysis.mode()
