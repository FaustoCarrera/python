#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys


def main(limit):
    limit = int(limit)
    total = 1
    i = 1
    while total < limit:
        if is_prime(i):
            print i,
            total += 1
        i += 1


def is_prime(n):
    """
    returns: true if n prime, else false
    """
    n *= 1.0
    for divisor in range(2, int(n**0.5) + 1):
        if n / divisor == int(n / divisor):
            return False
    return True


def get_arg(pos, default):
    try:
        val = sys.argv[pos]
    except:
        val = default
    return val

if __name__ == "__main__":
    limit = get_arg(1, 20)
    main(limit)
