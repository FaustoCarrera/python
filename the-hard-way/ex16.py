#!/usr/bin/env python

def main():
  print "We're going to erase everything"
  print "Hit Ctrl+c to cancel or enter the name of the file"
  print "Enter the name of the file to continue"
  filename = raw_input("> ")
  
  print "Opening the file...."
  target = open(filename, 'w')
  
  print "Truncating file"
  target.truncate()

  print "Enter a few new lines"
  l1 = raw_input("> ")
  l2 = raw_input("> ")
  l3 = raw_input("> ")

  print "Writing new lines to file"
  content = l1 + "\n" + l2 + "\n" + l3
  target.write(content)

  print "done"

if __name__ == '__main__':
  main()