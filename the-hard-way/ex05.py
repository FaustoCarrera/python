#!/usr/bin/env python
name = 'SilverDaemon'
age = 33
height = 70.6 #inches
weight = 165 #pounds
eyes = 'brown'
teeth = 'white'
hair = 'black'

print "Let's talk about %s." % name
print "He's %s inches tall or %f meters." % (height, round(height/39.3701, 2))
print "He's %d punds heavy." % weight
print "Actually that's not too heavy, thats %f kilograms" % (round(weight/2.20462, 2))
print "He's got %s eyes and %s hair." % (eyes, hair)
print "His teeth are usually %s depending on the coffee" % teeth
print "If I add %d, %d and %d I get %d" % (age, weight, height, age+weight+height)