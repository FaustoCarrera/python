#!/usr/bin/env python
from sys import argv

def read_text(filename):
	try:
		txt = open(filename)
		return txt.read()
	except :
		print 'The file %s could not be readed' % filename

def main():
	print "Write the name of the file you want to open"
	prompt = "> "
	filename = raw_input(prompt)
	print read_text(filename)

if __name__ == '__main__':
	main()