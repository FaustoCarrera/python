#!/usr/bin/env python
import sys

def main():
  args = sys.argv[1:]
  # check if we have arguments
  if not args :
    print "You must specify a file"
    sys.exit(1)
  # the iput file
  input_file = args[0]
  try :
    current_file = open(input_file, "r")
  except :
    print "%s cannot be opened" % input_file

  #print_all(current_file)
  #rewind(current_file)
  words = get_words(current_file)
  letters = get_letters(words)
  print "Total unique words: %d" % len(words)
  print letters

def print_all(file):
  print file.read()

def rewind(file):
  file.seek(0)

def get_words(file):
  words_list = []
  for line in file:
    words = line.split(' ')
    for word in words:
      word = word.replace(",", "").replace(".", "").lower().strip()
      if word not in words_list:
        if word != "":
          words_list.append(word)
  words_list.sort()
  return words_list

def get_letters(words):
  letters_list = []
  for word in words:
    l = word[0]
    if l not in letters_list:
      letters_list.append(l)
  return letters_list

if __name__ == '__main__':
  main()