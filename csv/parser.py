#!/usr/bin/env PYTHON
# -*- coding: utf-8 -*-
"""
Parse a csv file
default delimiter -> ,
"""

import csv

data = open('data.csv', 'r')
# reader = csv.reader(data, delimiter='\t')
reader = csv.reader(data)
for row in reader:
    print row
