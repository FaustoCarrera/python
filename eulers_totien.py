#!/usr/bin/env python
#-*- coding: utf-8 -*-

import sys
import fractions


def phi(n):
    p = 1
    n = int(n)
    for i in range(2, n):
        if fractions.gcd(i, n) == 1:
            p += 1
    return p

def get_arg(pos, default):
    try:
        val = sys.argv[pos]
    except:
        val = default
    return val

if __name__ == "__main__":
    n = get_arg(1, 100)
    print phi(n)
