#!/usr/bin/env python
"""
Read a playlist and move the mp3 files
"""

import sys
import os
import argparse
import shutil


def parse_playlist(filename):
    "parse playlist file"
    files_list = []
    source = '/home/silverdaemon/Music'
    destin = './music'
    playlist = open(filename, 'r')
    for line in playlist:
        line = line.replace('\r', '').replace('\n', '')
        if '#EXTINF' not in line:
            source_file = '%s/%s' % (source, line)
            destin_file = '%s/%s' % (destin, line)
            if os.path.isfile(source_file):
                files_list.append((source_file, destin_file))
    return files_list


def copy_files(files_list):
    "copy files"
    total_folders = 0
    total_files = 0
    for files in files_list:
        destin_parts = files[1].split('/')
        del destin_parts[-1]
        destin_path = r'/'.join(destin_parts)
        copy = True
        # create path
        try:
            if not os.path.exists(destin_path):
                os.makedirs(destin_path)
                total_folders += 1
        except Exception, e:
            copy = False
            print e
        # copy files
        if copy:
            print 'copying file %s' % files[0]
            # check if file exists
            if not os.path.isfile(files[1]):
                try:
                    shutil.copy(files[0], files[1])
                    total_files += 1
                except Exception, e:
                    print e
    print '%d folders created' % total_folders
    print '%d files copied' % total_files


def list_files(folder):
    files = []
    for root, subdirs, files_list in os.walk(folder):
        files = files_list
    return files


def main():
    "start script"
    args = arguments()
    # check if file exists
    source = args.source
    if os.path.isfile(source):
        # if the source is a file
        files = parse_playlist(source)
        copy_files(files)
    elif os.path.isdir(source):
        # if the source is a dir
        playlists = list_files(source)
        count = 1
        for playlist in playlists:
            print '-' * 25
            print 'copying playlist %d of %d' % (count, len(playlists))
            print '-' * 25
            playlist_path = './%s/%s' % (source, playlist)
            files = parse_playlist(playlist_path)
            copy_files(files)
            count += 1
    else:
        print 'error reading file %s' % source


def arguments():
    "Parse cli arguments"
    parser = argparse.ArgumentParser(
        prog=sys.argv[0], description='Script to export from playlist to mp3')
    parser.add_argument('--source', required=True, type=str,
                        help='playlist or folder with playlists to export', default=False)

    parsed_args = parser.parse_args()
    return parsed_args

if __name__ == '__main__':
    main()
