#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os
import urllib
import re
from bs4 import BeautifulSoup

def imgur(url, target):
    print "Loading url..."
    donwload_url = "https://i.imgur.com/"
    f = urllib.urlopen(url)
    html = f.read()
    f.close()

    try:
        if not os.path.exists(target):
            print "Creating destination folder..."
            os.makedirs(target)
    except:
        print "error could not create folder %s" % target
        sys.exit()

    print "Parsing html..."
    s = BeautifulSoup(html)
    posts = s.find_all('div', class_='post')
    for post in posts:
        # check if we have a url to retrieve
        try:
            href = post.a['href'].split('/')[-1]
            remote_image = donwload_url + href
            local_image = target + '/' + href
            download(local_image, remote_image)
        except:
            continue


def download(local_image, remote_image):
    # download the image
    try:
        print "downloading file %s saving to %s" % (remote_image, local_image)
        i = urllib.urlopen(remote_image)
        lf = open(local_image, 'w')
        lf.write(i.read())
        i.close()
        lf.close()
    except:
        print "error downloading file %s" % remote_image

def help():
    print "Usage: %s url target" % sys.argv[0]
    print "python %s http://imgur.com/top_images" % sys.argv[0]
    print "%s uses BeautifulSoup to parse html, you could donwload it from" % sys.argv[0]
    print "http://www.crummy.com/software/BeautifulSoup/"
    sys.exit();

if __name__ == '__main__':
    try:
        url = sys.argv[1]
        target = sys.argv[2]
    except:
        help()

    imgur(url, target)