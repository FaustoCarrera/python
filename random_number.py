#!/usr/bin/env python
#-*- coding: utf-8 -*-

import random

if __name__ == "__main__":
    random_number = random.randint(1, 100)
    if random_number % 2:
        print 'option A'
    else:
        print 'option B'
